\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{oneraposter}[2013/11/22 acme-poster personalisation]

\def\lang{english}
\def\langshort{en}
\def\pagelayout{portrait}
\def\headertitlewidth{.80\textwidth}

%% Loading memoir class
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{acme-poster}
}
\DeclareOption{english}{}
\DeclareOption{french}{\def\lang{french}\def\langshort{fr}}
\DeclareOption{portrait}{}
\DeclareOption{landscape}{\def\pagelayout{landscape}\def\headertitlewidth{.70\textwidth}}
\ProcessOptions\relax
\LoadClass[a0,\pagelayout]{acme-poster}

\RequirePackage{times}
\RequirePackage{courier}
\RequirePackage{helvet}
%\RequirePackage[\lang]{babel}
\RequirePackage{color}
\RequirePackage{graphicx}
\RequirePackage{ifthen}
\RequirePackage{tikz}
\RequirePackage{multicol}
\RequirePackage[a0paper,noheadfoot,includeall,marginparsep=0in,marginparwidth=0in,vmargin={-1in,0in},hmargin={-1in,1in}]{geometry}
\geometry{showframe}
\definecolor{oneralightblue}{cmyk}{0.15,0.05,0.0,0.0}
\definecolor{onerablue}{cmyk}{0.91,0.6,0.0,0.0}
\definecolor{oneradarkblue}{cmyk}{0.91,0.6,0.0,0.6}

% Header data definition
\def\@posterdate{}
\newcommand\posterdate[1]{\def\@posterdate{#1}}
\def\@postertitle{}
\newcommand\postertitle[1]{\def\@postertitle{#1}}
\def\@posteremail{}
\newcommand\posteremail[1]{\def\@posteremail{#1}}
\def\@posterdepartment{}
\newcommand\posterdepartment[1]{\def\@posterdepartment{#1}}

\def\neutralizetitle{\def\section{\@ifstar\@gobble\@gobble}}

%%% Classe authblk
\newcommand\Authfont{\normalfont}
\newcommand\Affilfont{\normalfont}
\newcommand\Authsep{, }
\newcommand\Authands{, and }
\newcommand\Authand{ and }
\newlength{\affilsep}\setlength{\affilsep}{1em}
\newlength{\@affilsep}
\newcounter{Maxaffil}
\setcounter{Maxaffil}{2}
\newcounter{authors}
\newcounter{affil}
\newif\ifnewaffil \newaffiltrue
\newcommand\AB@authnote[1]{\textsuperscript{\normalfont#1}}
\newcommand\AB@affilnote[1]{\textsuperscript{\normalfont#1}}
\providecommand\textsuperscript[1]{$^{#1}$}
\newcommand\AB@blk@and{\protect\Authfont\protect\AB@setsep}
\newcommand\AB@pand{\protect\and \protect\Authfont \protect\AB@setsep}
\@namedef{@sep1}{}
\@namedef{@sep2}{\Authand}
\newcommand\AB@affilsep{\protect\Affilfont}
\newcommand\AB@affilsepx{\protect\\\protect\Affilfont}
\newcommand\AB@setsep{\setlength{\@affilsep}{\affilsep}}
\newcommand\AB@resetsep{\setlength{\@affilsep}{\z@}}
\newcommand\AB@authlist{}
\newcommand\AB@affillist{}
\newcommand\AB@authors{}
\newcommand\AB@empty{}
\xdef\AB@author{\noexpand\AB@blk@and\@author}
%
\renewcommand\author[2][]%
      {\ifnewaffil\addtocounter{affil}{1}%
       \edef\AB@thenote{\arabic{affil}}\fi
      \if\relax#1\relax\def\AB@note{\AB@thenote}\else\def\AB@note{#1}%
        \setcounter{Maxaffil}{0}\fi
      \ifnum\value{authors}>1\relax
      \@namedef{@sep\number\c@authors}{\Authsep}\fi
      \addtocounter{authors}{1}%
      \begingroup
          \let\protect\@unexpandable@protect \let\and\AB@pand
          \def\thanks{\protect\thanks}\def\footnote{\protect\footnote}%
         \@temptokena=\expandafter{\AB@authors}%
         {\def\\{\protect\\[\@affilsep]\protect\Affilfont
              \protect\AB@resetsep}%
              \xdef\AB@author{\AB@blk@and#2}%
       \ifnewaffil\gdef\AB@las{}\gdef\AB@lasx{\protect\Authand}\gdef\AB@as{}%
           \xdef\AB@authors{\the\@temptokena\AB@blk@and}%
       \else
          \xdef\AB@authors{\the\@temptokena\AB@as\AB@au@str}%
          \global\let\AB@las\AB@lasx\gdef\AB@lasx{\protect\Authands}%
          \gdef\AB@as{\Authsep}%
       \fi
       \gdef\AB@au@str{#2}}%
         \@temptokena=\expandafter{\AB@authlist}%
         \let\\=\authorcr
         \xdef\AB@authlist{\the\@temptokena
           \protect\@nameuse{@sep\number\c@authors}%
           \protect\Authfont#2\AB@authnote{\AB@note}}%
      \endgroup
      \ifnum\value{authors}>2\relax
      \@namedef{@sep\number\c@authors}{\Authands}\fi
      \newaffilfalse
}
\newcommand\authorcr{\protect\\ \protect\Authfont \protect\AB@setsep}%
\newcommand\affil[2][]%
   {\newaffiltrue\let\AB@blk@and\AB@pand
      \if\relax#1\relax\def\AB@note{\AB@thenote}\else\def\AB@note{#1}%
        \setcounter{Maxaffil}{0}\fi
      \begingroup
        \let\protect\@unexpandable@protect
        \def\thanks{\protect\thanks}\def\footnote{\protect\footnote}%
        \@temptokena=\expandafter{\AB@authors}%
        {\def\\{\protect\\\protect\Affilfont}\xdef\AB@temp{#2}}%
         \xdef\AB@authors{\the\@temptokena\AB@las\AB@au@str
         \protect\\[\affilsep]\protect\Affilfont\AB@temp}%
         \gdef\AB@las{}\gdef\AB@au@str{}%
        {\def\\{, \ignorespaces}\xdef\AB@temp{#2}}%
        \@temptokena=\expandafter{\AB@affillist}%
        \xdef\AB@affillist{\the\@temptokena \AB@affilsep
          \AB@affilnote{\AB@note}\protect\Affilfont\AB@temp}%
      \endgroup
       \let\AB@affilsep\AB@affilsepx
}
\def\@author{}
\renewcommand\@author{\ifx\AB@affillist\AB@empty\AB@author\else
      \ifnum\value{affil}>\value{Maxaffil}\def\rlap##1{##1}%
    \AB@authlist\\[\affilsep]\AB@affillist
    \else  \AB@authors\fi\fi}
\let\AB@maketitle=\maketitle
\def\maketitle
  {{\renewenvironment{tabular}[2][]{\begin{center}}
                                   {\end{center}}
  \AB@maketitle}}
% Fin classe authblk

\newcommand{\makeposterheader}{
  \includegraphics[width=\textwidth]{oneraheader_\langshort.jpg}
   \vskip 10mm
   \noindent\makebox[\textwidth][c]{%
   \begin{minipage}[c]{\headertitlewidth}
     \vskip 6mm
     {\fontsize{72}{72}\selectfont \textcolor{onerablue}{\textbf{\@postertitle}}}
     \vskip 5mm
     {\fontsize{50}{50}\selectfont \textbf{\@author}}
   \end{minipage}}
   \hfill
   \vskip 15mm
   \textcolor{onerablue}{\rule{\textwidth}{0.5ex}}
   \vskip 13mm
}

