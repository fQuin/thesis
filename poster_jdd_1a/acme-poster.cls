% Format pour les posters.
% Time-stamp: <2009-10-28 16:23:43 cmauclai>

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{acme-poster}[2009/10/09 A class to typeset posters easily, like any other.]

\typeout{}
\typeout{######################}
\typeout{}
\typeout{Format pour faire des posters comme il en existe beaucoup.}
\typeout{Cedric Mauclair (DTIM) -- oct. 2009}
\typeout{}
\typeout{######################}
\typeout{}

\LoadClassWithOptions{sciposter}


% ==< Packages >================================================================

\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{dsfont}
\RequirePackage[amssymb,thinqspace]{SIunits}
\RequirePackage{color,graphicx}
\RequirePackage{multicol}
\RequirePackage{xifthen}

% ==> Packages <================================================================


% ==< Macros >==================================================================

\newsavebox{\dummybox}
\newlength{\dummyboxwidth}
\newlength{\postermargin}\setlength{\postermargin}{25mm}

% ——< Colors >——————————————————————————————————————————————————————————————————

\DefineNamedColor{named}{blueonera}{cmyk}{0.91,0.6,0.0,0.0}
\DefineNamedColor{named}{white}{rgb}{1., 1., 1.}
\DefineNamedColor{named}{black}{rgb}{0., 0., 0.}
\DefineNamedColor{named}{verylightgrey}{rgb}{.95, .95, .95}
\DefineNamedColor{named}{lightgrey}{rgb}{.8, .8, .8}

% ——< Title Boxes >—————————————————————————————————————————————————————————————

\newlength{\titleboxfboxrule}\setlength{\titleboxfboxrule}{3pt}
\newlength{\titleboxfboxsep}\setlength{\titleboxfboxsep}{2em}
\newlength{\titleboxcolumnsep}\setlength{\titleboxcolumnsep}{0em}

\newcommand{\titlebox}[3]{%
  \definecolor{titleboxrulecolor}{cmyk}{0.91,0.6,0.0,0.0}% XXX : ?
  \definecolor{titleboxfgcolor}{named}{black}%
  \definecolor{titleboxbgcolor}{named}{white}%
  \tikzstyle{titlebox}=[titleboxrulecolor,fill=titleboxbgcolor,line width=\titleboxfboxrule,rectangle,inner sep=\titleboxfboxsep,inner ysep=\titleboxfboxsep,draw,rounded corners=10pt]%
  \tikzstyle{title}=[titleboxrulecolor,fill=titleboxbgcolor,line width=\titleboxfboxrule,minimum height=3em,draw,rounded corners=10pt]%
%
  \bigskip\noindent%
   \begin{center}
  \begin{tikzpicture}%
    \node[titlebox] (box){%
      \begin{minipage}[t]{#3 - \titleboxfboxsep - \titleboxfboxsep-0.5em}%
        \vspace{.5em}
        \color{titleboxfgcolor}%
        \Large%
        #2%
      \end{minipage}%
    };%
    \node[title] at (box.north) {\huge\textbf{~#1~~}};%
  \end{tikzpicture}%
   \end{center}
  \bigskip%
}

% ——< Boxes >———————————————————————————————————————————————————————————————————

\newlength{\boxfboxrule}\setlength{\boxfboxrule}{4pt}
\newlength{\boxfboxsep}\setlength{\boxfboxsep}{1.2em}
\newlength{\boxcolumnsep}\setlength{\boxcolumnsep}{1.4em}
\definecolor{boxrulecolor}{named}{blueonera}
\definecolor{boxfgcolor}{named}{black}
\definecolor{boxbgcolor}{named}{white}

\newenvironment{colorframebox}[2][\linewidth]{%
  \setlength{\columnsep}{\boxcolumnsep}%
  \setlength{\fboxsep}{\boxfboxsep}%
  \setlength{\fboxrule}{\boxfboxrule}%
  \setlength{\dummyboxwidth}{#1-2\fboxsep-2\fboxrule}%
  \begin{lrbox}{\dummybox}%
    \begin{minipage}{\dummyboxwidth}
      \ifthenelse{\isempty{#2}}{}{%
        \section{#2}}}
{   \end{minipage}%
  \end{lrbox}%
  \raisebox{-\depth}{%
    \fcolorbox{boxrulecolor}{boxbgcolor}{%
      \color{boxfgcolor}\usebox{\dummybox}}}\bigskip}

% ——< Small boxes >—————————————————————————————————————————————————————————————

\newlength{\smallboxfboxrule}\setlength{\smallboxfboxrule}{2pt}
\newlength{\smallboxfboxsep}\setlength{\smallboxfboxsep}{.4em}
\definecolor{smallboxrulecolor}{named}{black}
\definecolor{smallboxfgcolor}{named}{black}
\definecolor{smallboxbgcolor}{named}{white}

\newenvironment{smallbox}[1][\linewidth]{%
  \setlength{\fboxsep}{\smallboxfboxsep}%
  \setlength{\fboxrule}{\smallboxfboxrule}%
  \setlength{\dummyboxwidth}{#1-2\fboxsep-2\fboxrule}%
  \begin{lrbox}{\dummybox}%
    \begin{minipage}{\dummyboxwidth}}
{   \end{minipage}%
  \end{lrbox}%
  \raisebox{-\depth}{%
    \fcolorbox{smallboxrulecolor}{smallboxbgcolor}{%
      \color{smallboxfgcolor}\usebox{\dummybox}}}}

\newenvironment{smallcenteredbox}[1][\linewidth]{%
  \begin{smallbox}[#1]
    \begin{center}
}
{   \end{center}
  \end{smallbox}}

% ——< Sections >————————————————————————————————————————————————————————————————

\newlength{\sectionfboxrule}\setlength{\sectionfboxrule}{2pt}
\newlength{\sectionfboxsep}\setlength{\sectionfboxsep}{1ex}
\definecolor{sectionrulecolor}{named}{blueonera}
\definecolor{sectionfgcolor}{named}{blueonera}
\definecolor{sectionbgcolor}{named}{verylightgrey}
\newcommand{\sectionstyle}{\large\bfseries\color{sectionfgcolor}}

\renewcommand{\section}[1]{%
  \ifthenelse{\equal{#1}{*}}{}{%
  \hfill%
  \setlength{\fboxsep}{\sectionfboxsep}%
  \setlength{\fboxrule}{\sectionfboxrule}%
  \fcolorbox{sectionrulecolor}{sectionbgcolor}{%
    \hbox to .6\linewidth{\hfill\sectionstyle\relax#1\hfill}}%
  \hfill%
  \vspace{2ex}\par}}

% ——< Miscelaneous >————————————————————————————————————————————————————————————

\setlength{\columnseprule}{0pt}
\setlength{\columnsep}{25mm}

\parindent0mm
\parskip1.5ex plus0.5ex minus 0.5ex
\pagestyle{empty}

\newcommand{\keyword}[2][blueonera]{{\bfseries\color[named]{#1}#2}}

\long\outer\def\groupe #1. #2\par{\medbreak
  \noindent{\bfseries#1}\par\vskip1ex{#2}\par%
  \ifdim\lastskip<\medskipamount \removelastskip\penalty55\medskip\fi}

% ——< Lines >———————————————————————————————————————————————————————————————————

\newcommand{\drawLine}[1][.4\linewidth]{
  \removelastskip\vspace{\medskipamount}
  \hbox to \linewidth{\hfill\vrule width #1 height 1pt depth 1pt\hfill}
  \vspace{\bigskipamount}
}

% ==> Macros <==================================================================

%%% Local Variables:
%%% fill-column: 80
%%% eval:(set-regexp-face "^% =+\\|==+$" 'VioletRed4-bold-italic)
%%% eval:(set-regexp-face "^% —+\\|——+$" 'LimeGreen-bold-italic)
%%% eval:(set-regexp-face "\\(<\\|>\\) .+ \\(>\\|<\\)=" 'VioletRed4-bold-italic)
%%% eval:(set-regexp-face "\\(<\\|>\\) .+ \\(>\\|<\\)—" 'LimeGreen-bold-italic)
%%% End:
