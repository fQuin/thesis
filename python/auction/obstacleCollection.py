import numpy as np
# from robot import Robot
# from space_graph import spaceGraph


def equation_droite(x, y):
    # trouve l'équation de la droite passant par deux points
    a = (x[1]-y[1])/(x[0]-y[0])
    b = y[1] - a*y[0]
    return (a, b)


class obstacleCollection:
    def __init__(self, n, min_l, max_l, max_iter=1e3):
        self.n = n
        self.min_l = min_l
        self.max_l = max_l
        self.max_iter = max_iter
        self.collection = []

    def checkInstersect(self, space_graph, x, y):
        a, b = equation_droite(x, y)
        for edge in space_graph.graph.edges:
            c, d = equation_droite(space_graph.pos_2D[edge[0]],
                                   space_graph.pos_2D[edge[1]])
            s = (d-b)/(a-c)
            if s >= min(x[0], y[0]) and s <= max(x[0], y[0]):
                return True
        return False

    def buildObs(self, space_graph):
        nb_iter = 0
        nb_obst = 0
        while nb_iter < self.max_iter and nb_obst < self.n:
            x1 = np.random.uniform(space_graph.pos_2D[:, 0].min(),
                                   space_graph.pos_2D[:, 0].max())
            x2 = np.random.uniform(space_graph.pos_2D[:, 1].min(),
                                   space_graph.pos_2D[:, 1].max())
            x = (x1, x2)
            y1 = np.random.uniform(max(-1, x[0]-self.max_l),
                                   min(1, x[0]+self.max_l))
            y2 = np.random.uniform(max(-1, x[1]-self.max_l),
                                   min(1, x[1]+self.max_l))
            y = np.array([y1, y2])
            # if not self.checkInstersect(space_graph, x, y):
            nb_obst += 1
            self.collection.append((x, y))
            nb_iter += 1

    def totxt(self, path):
        file1 = open(path, "w")
        for obstacle in self.collection:
            file1.writelines(str(obstacle))
        file1.close()

    def plot(self, path, plt=None, save=True, close=True,
             o_marker='x', nb_marker=10):
        for obstacle in self.collection:
            a, b = equation_droite(*obstacle)
            x = np.linspace(obstacle[0][0], obstacle[1][0], nb_marker)
            y = a*x+b
            plt.plot(x, y, marker=o_marker)
        if save:
            plt.savefig(path + '/space_graph'+'.pdf')
        if close:
            plt.close()
        else:
            return plt


if __name__ == "__main__":
    R = 5
    # G = spaceGraph([], graph_type='read',
    #                input_graph=nx.sedgewick_maze_graph())
    robots = []
    # for r in range(R):
    #     robots.append(Robot(r, np.random.choice(5), G))
    # comG = comGraph(R)
    # comG.buildFromRobots(robots)
    for r in robots:
        print(r.ind, np.round(r.pos, 3))
    # print(comG.graph.edges)
