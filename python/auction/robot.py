from space_graph import spaceGraph
from com_graph import comGraph
import networkx as nx
import numpy as np
from geometry import compute_dist, equation_droite, find_new_point


# A class that represents a robot
class Robot:
    def __init__(self, ind, node, G, com_range=1, velocity=1, max_energy=1,
                 energy_cons=0.05, standing_cons=1e-4):
        """
        Defines a robot and initialize its data
        :ind: An integer , must be unique among the robotic team
        :node: An integer , representing the starting node of the
        :G: A spaceGraph , modeling the surveillance area
        :com_range: An real number , representing radius of robot's coms range
        :velocity: An real number , representing robot's movement speed
        :max_energy: An real number , representing robot's starting energy
        :energy_cons: An real number , representing robot's energy consumption
        to move one distance unit
        :standing_cons: An real number ,representing robot's energy consumption
        to stand still
        """
        self.ind = ind
        if isinstance(node, int):
            self.pos = G.pos_2D[node]
            self.node = node
        else:
            self.pos = G.pos_2D[tuple(node)]
            self.node = tuple(node)
        self.last_node = node
        self.com_range = com_range
        self.velocity = velocity
        self.energy = max_energy
        self.energy_cons = energy_cons
        self.standing_cons = standing_cons
        self.com_suscripters = []
        self.auctions = {n: {} for n in G.graph.nodes}
        self.team_costs = {n: {} for n in G.graph.nodes}
        self.team_dest = {}
        self.team_paths = {ind: (0, [])}
        self.team_pos = {ind: (0, self.pos)}
        self.team_node = {ind: (0, self.node)}
        self.last_visits = {w: 0 for w in G.graph.nodes}
        self.destination = None
        self.path = []

    def move(self, G, present_time, time_increment=1, verbose=False):
        """
        Makes the robot move along the edges of an area graph, while
        following its path
        :G: A spaceGraph , modeling the surveillance area
        :present_time: An integer , corresponding to current time step
        :time_increment: A positive real number , corresponding the increment
        in time for each time step
        :verbose: A boolean , indicating if verbose mode in on or not radius
        of robot's coms range
        :return: An boolean , True if robot moved successfully, False either
        """
        if self.energy <= 0:
            return False
        begin_pos = tuple(self.pos)
        destination_pos = G.pos_2D[self.destination]
        if compute_dist(self.pos, destination_pos) <= 1e-5:
            G.last_visits[self.destination] = present_time
            self.last_node = self.destination
            self.destination = None
            self.path = []
            self.team_pos[self.ind] = (present_time, self.pos)
            self.team_node[self.ind] = (present_time, self.node)
            self.team_paths[self.ind] = (present_time, self.path)
            return True
        next_node_pos = G.pos_2D[self.path[0]]
        # then if the robot is far from the next node on its path,
        # make it move toward that next node
        if len(self.path) > 0 and compute_dist(self.pos, next_node_pos) > self.velocity:
            if self.last_node in G.graph[self.path[0]]:
                eq = equation_droite(self.pos, next_node_pos)
                p1, p2 = find_new_point(self.pos, eq, self.velocity)
                if compute_dist(p1, next_node_pos) < compute_dist(p2, next_node_pos):
                    self.pos = p1
                else:
                    self.pos = p2
        # sinon, si le robot est assez proche du noeud pour l'atteindre en une
        # itération, changer sa position pour le noeud.
        elif (len(self.path) > 0 and compute_dist(self.pos, next_node_pos)<=self.velocity):
            if self.last_node in G.graph[self.path[0]]:
                new_pos_x = next_node_pos[0]
                new_pos_y = next_node_pos[1]
                self.pos = [new_pos_x, new_pos_y]
        end_pos = tuple(self.pos)
        if verbose:
            print('begin pos : ', begin_pos, ' ; end_pos : ', end_pos)
        self.energy -= time_increment*self.velocity*self.energy_cons
        # if the robot is very close to a node, round up his position
        # to exactly on the node
        if compute_dist(self.pos, next_node_pos) < 1e-4:
            if len(self.path) > 0:
                self.pos = list(next_node_pos)
                self.node = self.path[0]
                self.last_node = self.node
                self.path = self.path[1:]
        # if the robot is very close to a its destination, update the node
        # idleness and reset the robots destination and path
        if compute_dist(self.pos, destination_pos) < 1e-4:
            G.last_visits[self.destination] = present_time
            self.last_visits[self.destination] = present_time
            self.last_node = self.destination
            self.destination = None
            self.path = []
        self.team_pos[self.ind] = (present_time, self.pos)
        self.team_node[self.ind] = (present_time, self.node)
        self.team_paths[self.ind] = (present_time, self.path)
        return True

    def update_com_suscripters(self, robots):
        """
        Updates the list of robot in communications range
        :robots: A list of robots , the robots of the multi-robotic team
        """
        are_in_range = []
        for robot in robots:
            if not robot.ind == self.ind:
                a = np.array(robot.pos)
                b = np.array(self.pos)
                if np.linalg.norm(a-b) <= self.com_range:
                    are_in_range.append(robot)
        self.com_suscripters = are_in_range

    def compute_auctions(self, present_time, G, C, robots, obstacles,
                         method=["IDLENESS"], weights=[1]*3):
        """
        Computes the auctions of the robot for all waypoints
        :present_time: An integer , corresponding to current time step
        :G: A spaceGraph , modeling the surveillance area
        :C: A comGraph , modeling the communication among team members
        :robots: A list of robots , the robots of the multi-robotic team
        :obstacles: A list of obstacles , obstacles in the area, defined by the
        collection attribute of class Obstacles
        :method: A list of strings , the name of the method used to compute
        bids. Must be a combination of "IDLENESS", "ENERGY", "COMS", "MUL",
        and "MIN", OR JUST "Pareto"
        :return: A 3-tuple of dics , corresponding to the values of the
        idleness terms, energy terms and coms terms
        """
        if self.destination is None:
            if "IDLENESS" in method or "Pareto" in method:
                idle_costs = self.compute_idle_cost(present_time, G,
                                                    ("MUL" in method))
                idle_costs_for_data = idle_costs.copy()
            else:
                idle_costs = {}
                idle_costs_for_data = {}
            if "ENERGY" in method or "Pareto" in method:
                ener_costs = self.compute_ener_cost(G,
                                                    ("MUL" in method),
                                                    ("Pareto" in method))
                ener_costs_for_data = ener_costs.copy()
                reach_costs = self.compute_reachability_cost(G)
            else:
                ener_costs = {}
                ener_costs_for_data = {}
                reach_costs = {}
            if "COMS" in method or "Pareto" in method:
                coms_costs = self.compute_coms_cost(present_time, C,
                                                    G, robots,
                                                    obstacles,
                                                    ("MUL" in method),
                                                    ("MIN" in method),
                                                    ("Pareto" in method))
                coms_costs_for_data = coms_costs.copy()

            else:
                coms_costs = {}
                coms_costs_for_data = {}
            if "MUL" in method:
                res = {key: weights[2]*coms_costs.get(key, 0)
                       * weights[0]*idle_costs.get(key, 0)
                       * weights[1]*ener_costs.get(key, 0)
                       - reach_costs.get(key, 0)
                       + np.random.uniform(-1e-8, 1e-8)
                       for key in idle_costs}
            else:
                res = {key: weights[0]*idle_costs.get(key, 0)
                       + weights[1]*ener_costs.get(key, 0)
                       + weights[2]*coms_costs.get(key, 0)
                       - reach_costs.get(key, 0)
                       + np.random.uniform(-1e-8, 1e-8)
                       for key in idle_costs}
            for key in res:
                self.auctions[key][self.ind] = res[key]
                self.team_costs[key][self.ind] = (idle_costs.get(key, 0),
                                                  ener_costs.get(key, 0),
                                                  coms_costs.get(key, 0))
            return idle_costs_for_data, ener_costs_for_data, coms_costs_for_data
        else:
            self.auctions = {}

    def receive_auctions(self, auctions, loss_proba=0):
        """
        Manages the receiving of teammate's auctions
        :auctions: A dic , corresponding to the auctions of a teammate.
        Obtained using the auctions attribute of another robot.
        :loss_proba: A float in [0, 1[ , the probability of losing the message
        """
        for waypoint in self.auctions:
            for robot in auctions[waypoint]:
                if not np.random.uniform() < loss_proba:
                    self.auctions[waypoint][robot] = auctions[waypoint][robot]

    def receive_costs(self, costs, loss_proba=0):
        """
        Manages the receiving of teammate's knowledge of any teammate's costs
        :costs: A dic of dic , corresponding to the costs of a teammate
        per waypoint. Obtained using the team_costs attribute of another robot.
        :loss_proba: A float in [0, 1[ , the probability of losing the message
        """
        for waypoint in self.team_costs:
            for robot in costs[waypoint]:
                if not np.random.uniform() < loss_proba:
                    self.team_costs[waypoint][robot] = costs[waypoint][robot]

    def receive_paths(self, team_paths, loss_proba=0):
        """
        Manages the receiving of teammate's paths and knowledge of any
        teammate's path
        :team_paths: A dic of dic , corresponding to the path of a teammate and
        paths of over teammates it is aware of. Obtained using the team_paths
        attribute of another robot.
        :loss_proba: A float in [0, 1[ , the probability of losing the message
        """
        for robot in team_paths:
            if self.team_paths.get(robot, (0, 0))[0] < team_paths[robot][0]:
                if not robot == self.ind:
                    self.team_paths[robot] = team_paths[robot]

    def receive_pos(self, team_pos, team_node, loss_proba=0):
        """
        Manages the receiving of teammate's pos and knowledge of any
        teammate's pos
        :team_pos: A dic of dic , corresponding to the pos of a teammate and
        pos of over teammates it is aware of. Obtained using the team_pos
        attribute of another robot.
        :loss_proba: A float in [0, 1[ , the probability of losing the message
        """
        for robot in team_pos:
            if self.team_pos.get(robot, (0, 0))[0] < team_pos[robot][0]:
                if not robot == self.ind:
                    self.team_pos[robot] = team_pos[robot]
                    self.team_node[robot] = team_node[robot]

    def receive_destination(self, team_dest, loss_proba=0):
        """
        Manages the receiving of teammate's destination and knowledge of any
        teammate's destination
        :team_dest: A dic of dic , corresponding to the destination of a
        teammate and destination of over teammates it is aware of. Obtained
        using the team_dest attribute of another robot.
        :loss_proba: A float in [0, 1[ , the probability of losing the message
        """
        for waypoint in team_dest:
            self.team_dest[waypoint] = max(self.team_dest.get(waypoint, 0),
                                           team_dest[waypoint])

    def receive_last_visits(self, last_visits, loss_proba=0):
        """
        Manages the receiving of teammate's knowledge about waypoints' last
        visit date
        :last_visits: A dic , corresponding to the last visit date known to an
        emitting robot for each waypoint
        :loss_proba: A float in [0, 1[ , the probability of losing the message
        """
        for waypoint in last_visits:
            self.last_visits[waypoint] = max(self.last_visits[waypoint],
                                             last_visits[waypoint])

    def motion_planning(self, graph, node):
        """
        Finds the shortest path to go from robot's present position to a node
        :graph: A nx graph , corresponding to the graph attribute of the
        spaceGraph representing the area
        :node: An integer , the destination node
        """
        if self.destination is not None:
            path = nx.dijkstra_path(graph, source=self.node,
                                    target=node, weight='weight')[1:]
            self.path = path

    def update_team_dest(self, present_time):
        """
        Updates the robot's knowledge of self and teammates destination.
        :present_time: An integer , corresponding to current time step
        """
        to_del = []
        for dest in self.team_dest:
            if self.team_dest[dest] <= present_time:
                to_del.append(dest)
        for dest in to_del:
            del self.team_dest[dest]

    def update_team_pos_and_path(self, G, present_time):
        """
        Updates the robot's knowledge of self and teammates position and path
        :G: A spaceGraph , modeling the surveillance area.
        :present_time: An integer , corresponding to current time step
        """
        for robot, (timer, path) in self.team_paths.items():
            pos = self.team_pos[robot][1]
            if len(path) > 0 and present_time > timer+1:
                next_node_pos = G.pos_2D[path[0]]
                if compute_dist(pos, next_node_pos) > self.velocity:
                    eq = equation_droite(pos, next_node_pos)
                    p1, p2 = find_new_point(pos, eq, self.velocity)
                    if compute_dist(p1, next_node_pos) < compute_dist(p2, next_node_pos):
                        self.team_pos[robot] = (present_time, p1)
                    else:
                        self.team_pos[robot] = (present_time, p2)
                else:
                    new_pos_x = next_node_pos[0]
                    new_pos_y = next_node_pos[1]
                    self.team_pos[robot] = (present_time,
                                            [new_pos_x, new_pos_y])
                pos = self.team_pos[robot][1]
                if compute_dist(pos, next_node_pos) < 1e-4:
                    self.team_pos[robot] = (present_time, tuple(next_node_pos))
                    self.team_paths[robot] = (self.team_paths[robot][0],
                                              self.team_paths[robot][1][1:])

    def predict_pos(self, robot, futur_time, G):
        """
        Predicts the position of teammates at a time in the futur based on self
        knowledge about teammates' current position and path
        :robot: A integer , id of a teammate
        :futur_time: An integer , number of time steps in the futur
        :G: A spaceGraph , modeling the surveillance area.
        """
        past_time, path = self.team_paths[robot]
        pos = self.team_pos[robot][1]
        time = past_time
        while len(path) > 0 and time < futur_time:
            next_node_pos = G.pos_2D[path[0]]
            if compute_dist(pos, next_node_pos) > self.velocity:
                eq = equation_droite(pos, next_node_pos)
                p1, p2 = find_new_point(pos, eq, self.velocity)
                if compute_dist(p1, next_node_pos) < compute_dist(p2, next_node_pos):
                    pos = p1
                else:
                    pos = p2
            else:
                new_pos_x = next_node_pos[0]
                new_pos_y = next_node_pos[1]
                pos = [new_pos_x, new_pos_y]
            if compute_dist(pos, next_node_pos) < 1e-4:
                pos = tuple(next_node_pos)
                path = path[1:]
            time += 1
        return pos

    def is_pareto_efficient(self, costs, return_mask=True):
        """
        Find the pareto-efficient points
        :param costs: An (n_points, n_costs) array
        :param return_mask: True to return a mask
        :return: An array of indices of pareto-efficient points.
            If return_mask is True, this will be an (n_points, ) boolean array
            Otherwise it will be a (n_efficient_points, ) integer array of
            indices.
        """
        is_efficient = np.arange(costs.shape[0])
        n_points = costs.shape[0]
        next_point_index = 0  # Next index in the is_efficient
        # array to search for
        while next_point_index < len(costs):
            nondominated_point_mask = np.any(costs > costs[next_point_index],
                                             axis=1)
            nondominated_point_mask[next_point_index] = True
            is_efficient = is_efficient[nondominated_point_mask]
            # Remove dominated points
            costs = costs[nondominated_point_mask]
            next_point_index = np.sum(nondominated_point_mask[:next_point_index])+1
        if return_mask:
            is_efficient_mask = np.zeros(n_points, dtype=bool)
            is_efficient_mask[is_efficient] = True
            return is_efficient_mask
        else:
            return is_efficient

    def pareto_alloc(self, present_time, G, top_rank=0,
                     assigned_ws=[], assigned_rs=[]):
        """
        Proceeds to the task allocation using Pareto optimality
        :present_time: An integer , corresponding to current time step
        :G: A spaceGraph , modeling the surveillance area
        :top_rank: An integer, the best rank of tasks to consider
        :assigned_ws: A list of integers, corresponding to already
        assigned waypoints
        :assigned_rs: A list of integers, corresponding to already
        assigned robots id
        """
        indic = {}
        arrdic = []
        cpt = 0
        for w in self.team_costs:
            if w not in assigned_ws:
                for r in self.team_costs[w]:
                    if r not in assigned_rs:
                        indic[cpt] = (w, r)
                        # Record the indices of each allocation
                        cpt += 1
                        arrdic.append(self.team_costs[w][r])
                        # Build of list of costs
        pareto = self.is_pareto_efficient(np.array(arrdic))
        for ind, n in enumerate(pareto):
            if n:
                assigned_ws.append(indic[ind][0])
                assigned_rs.append(indic[ind][1])
        self_front = []
        for ind, b in enumerate(pareto):
            if b and indic[ind][1] == self.ind:
                self_front.append(indic[ind][0])
        if len(self_front) == 0 and top_rank < 10:
            top_rank += 1
            self.pareto_alloc(present_time, G, top_rank=top_rank,
                              assigned_ws=assigned_ws, assigned_rs=assigned_rs)
        else:
            if len(self_front) == 1:
                self.destination = self_front[0]
            else:
                self_idle = {}
                for w in self.team_costs:
                    for r in self.team_costs[w]:
                        if r == self.ind:
                            self_idle[w] = self.team_costs[w][r][0]
                max_idle = 0
                for w in self_idle:
                    if self_idle[w] > max_idle:
                        arg_max = w
                        max_idle = self_idle[w]
                self.destination = arg_max

        self.motion_planning(G.graph, self.destination)
        if self.destination is not None:
            path_cost = G.path_length(self.path)
            self.team_dest[self.destination] = present_time + path_cost
            self.team_pos[self.ind] = (present_time, self.pos)
            self.team_node[self.ind] = (present_time, self.node)
            self.team_paths[self.ind] = (present_time, self.path)

    def task_alloc(self, G, present_time, auction_period):
        """
        Proceeds to the task allocation using single-item bidding scheme
        :G: A spaceGraph , modeling the surveillance area
        :present_time: An integer , corresponding to current time step
        :auction_period: An integer, number of time periods between two auction
        rounds
        """
        if present_time % auction_period == 0:
            tmp_alloc = {}
            dic = {}
            for waypoint in self.auctions:
                dic[waypoint] = []
                for robot in self.auctions[waypoint]:
                    dic[waypoint].append((self.auctions[waypoint][robot],
                                          robot))
            nb_robot = len(dic[next(iter(dic))])
            for iteration in range(nb_robot):
                lis = []
                for w, bids in dic.items():
                    for bid in bids:
                        lis.append((*bid, w))
                lis.sort()
                r_max = lis[-1][1]
                w_max = lis[-1][2]
                tmp_alloc[r_max] = w_max
                del dic[w_max]
                for w in dic.keys():
                    for bid in dic[w]:
                        if bid[1] == r_max:
                            dic[w].remove(bid)
            self.destination = tmp_alloc[self.ind]
            self.motion_planning(G.graph, self.destination)
            if self.destination is not None:
                path_cost = G.path_length(self.path)
                self.team_dest[self.destination] = present_time + path_cost
                self.team_pos[self.ind] = (present_time, self.pos)
                self.team_node[self.ind] = (present_time, self.node)
                self.team_paths[self.ind] = (present_time, self.path)

    def compute_ener_cost(self, G, ismul=False, ispareto=False):
        """
        Computes the energy term
        :G: A spaceGraph , modeling the surveillance area
        :ismul: An boolean , indicating if bid computation uses multiplication
        (E*I*C) form or standard form (C*(I+E))
        :ispareto: An boolean , indicating if uses Pareto or not
        """
        # Energy term
        ener_costs = {w: 0 for w in G.graph.nodes}
        if not self.energy == 0:
            for waypoint in G.graph.nodes:
                path = nx.dijkstra_path(G.graph, source=self.node,
                                        target=waypoint, weight='weight')
                dist = max(G.min_weight/2, G.path_length(path))
                mul_coef = (G.min_weight*self.energy_cons)/2
                ener_costs[waypoint] += self.energy/(dist*self.energy_cons) * mul_coef
                if ismul:
                    ener_costs[waypoint] = self.energy/dist
                if ispareto:
                    ener_costs[waypoint] = self.energy/(dist*self.energy_cons)
        return ener_costs

    def compute_reachability_cost(self, G):
        """
        Computes a very large penalty if the robot doesn't have enougth energy
        to complete the path to the considered waypoint
        :G: A spaceGraph , modeling the surveillance area
        """
        reach_costs = {}
        for waypoint in G.graph.nodes:
            path = nx.dijkstra_path(G.graph, source=self.node,
                                    target=waypoint, weight='weight')
            dist = G.path_length(path)
            if dist*self.energy_cons <= self.energy:
                reach_costs[waypoint] = 0
            else:
                reach_costs[waypoint] = 1e4
        return reach_costs

    def compute_coms_cost(self, present_time, C, G,
                          robots, obstacles, ismul=False, ismin=False,
                          ispareto=False):
        """
        Computes the communication maintenance term
        to complete the path to the considered waypoint
        :present_time: An integer , corresponding to current time step
        :C: A comGraph , modeling the communication among team members
        :G: A spaceGraph , modeling the surveillance area
        :robots: A list of robots , the robots of the multi-robotic team
        :obstacles: A list of obstacles , obstacles in the area, defined by the
        collection attribute of class Obstacles
        :ismul: An boolean , indicating if bid computation uses multiplication
        (E*I*C) form or standard form (C*(I+E))
        :ismin: An boolean , indicating if bid computation uses the inverse
        communication term ensuring that robots stay far away from each other
        :ispareto: An boolean , indicating if uses Pareto or not
        """
        nb_components = nx.number_connected_components(C.graph)
        coms_costs = {}
        true_pos = self.pos
        for waypoint in G.graph.nodes:
            path = nx.dijkstra_path(G.graph, source=self.node,
                                    target=waypoint, weight='weight')
            travel_time = G.path_length(path)/self.velocity
            dic_pos = {}
            for r in self.team_paths:
                if not r == self.ind:
                    dic_pos[r] = self.predict_pos(r, present_time+travel_time,
                                                  G)
            dic_pos[self.ind] = G.pos_2D[waypoint]
            tmp_coms_graph = comGraph(len(robots))
            tmp_coms_graph.buildFromRobots(robots, dic_pos, obstacles)
            futur_nb_cc = nx.number_connected_components(tmp_coms_graph.graph)
            futur_nb_cc = max(futur_nb_cc, 1)
            if ismul:
                coms_costs[waypoint] = 1./futur_nb_cc
                # nb_components/futur_nb_cc
            elif ismin:
                coms_costs[waypoint] = (futur_nb_cc-nb_components)*(10)
            elif ispareto:
                coms_costs[waypoint] = 1./futur_nb_cc
            else:
                coms_costs[waypoint] = nb_components/futur_nb_cc
        self.pos = true_pos
        return coms_costs

    def compute_idle_cost(self, present_time, G, ismul=False):
        """
        Computes the waypoint's idleness term
        to complete the path to the considered waypoint
        :present_time: An integer , corresponding to current time step
        :G: A spaceGraph , modeling the surveillance area
        :ismul: An boolean , indicating if bid computation uses multiplication
        (E*I*C) form or standard form (C*(I+E))
        """
        # Idleness term
        idle_costs = {w: 0 for w in G.graph.nodes}
        for waypoint in G.waypoints:
            path = nx.dijkstra_path(G.graph, source=self.node,
                                    target=waypoint, weight='weight')
            dist = G.path_length(path)
            if present_time > 0:
                if not dist == 0:
                    if waypoint not in self.team_dest:
                        idle_costs[waypoint] = (1
                                                + (present_time
                                                   - self.last_visits[
                                                       waypoint])
                                                / dist
                                                * self.velocity)
                        if ismul:
                            idle_costs[waypoint] = (dist*self.velocity
                                                    + present_time
                                                    - self.last_visits[
                                                        waypoint])
                    else:
                        idle_costs[waypoint] = abs(1
                                                   + (
                                                       - self.team_dest[
                                                           waypoint])
                                                   / dist
                                                   * self.velocity)
                        if ismul:
                            idle_costs[waypoint] = (dist
                                                    * self.velocity
                                                    - self.team_dest[
                                                        waypoint])
                else:
                    idle_costs[waypoint] = 2
            else:
                if not dist == 0:
                    idle_costs[waypoint] = self.velocity/dist
                else:
                    idle_costs[waypoint] = 0
        return idle_costs

    def print_pos(self):
        print(self.pos)

    def print_ener(self):
        print(self.energy)


if __name__ == "__main__":
    N, R, W = 10, 3, 5
    G = spaceGraph(N, R, W)
    r1 = Robot(0, 0, G)
    r2 = Robot(0, 1, G)
