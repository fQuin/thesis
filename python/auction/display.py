import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


# Set parameters
N = 10  # number of nodes
K = 4  # number of nearest neightbors connected
P = 0.5  # probability of creating a shortcut
W = 5  # number of waypoints
R = 3  # number of robots
T = 400  # number of time periods
P = 5   # auction delay
I = 10 # number of instances with each param
i = 0
LOSS = 0.0
methods = ["IDLENESS", "IDLENESSENERGY",
           "IDLENESSENERGYCOMSMUL", "IDLENESSENERGYCOMS",
           "IDLENESSENERGYCOMSMIN", "Pareto"]
methods_name = ["IDLENESS", "IDLENESSENERGY",
                "IDLENESSENERGYCOMSMUL", "IDLENESSENERGYCOMS",
                "IDLENESSENERGYCOMSMIN", "Pareto"]
labels = {"IDLENESS": "Idleness",
          "IDLENESSENERGY": "Idleness and Energy",
          "IDLENESSENERGYCOMS": "Idleness and Energy and Coms",
          "IDLENESSENERGYNORM": "Idleness and Energy Normalized",
          "IDLENESSENERGYCOMSNORM": "Idleness and Energy Normalized, Coms not Normalized",
          "IDLENESSENERGYCOMSNORMALL": "Idleness and Energy and Coms Normalized",
          "IDLENESSENERGYCOMSNORMMUL": "Idleness and Energy Normalized multplied by Coms",
          "IDLENESSENERGYCOMSMUL": "Idleness and Energy multplied by Coms",
          "IDLENESSENERGYCOMSMIN": "Reversed coms",
          "Pareto": "Pareto"}
colors = {"IDLENESS": "#1f77b4",
          "IDLENESSENERGY": "#ff7f0e", "IDLENESSENERGYCOMS": "#2ca02c",
          "IDLENESSENERGYNORM": "#d62728",
          "IDLENESSENERGYCOMSNORM": "#9467bd",
          "IDLENESSENERGYCOMSNORMALL": "gold",
          "IDLENESSENERGYCOMSNORMMUL": "pink",
          "IDLENESSENERGYCOMSMUL": "cyan",
          "IDLENESSENERGYCOMSMIN": "r",
          "Pareto": "k"}
markers = {"IDLENESS": "",
           "IDLENESSENERGY": "",
           "IDLENESSENERGYCOMS": "",
           "IDLENESSENERGYCOMSNORMMUL": "",
           "IDLENESSENERGYNORM": "",
           "IDLENESSENERGYCOMSNORM": "",
           "IDLENESSENERGYCOMSNORMALL": "",
           "IDLENESSENERGYCOMSMUL": "",
           "IDLENESSENERGYCOMSMIN": "",
           "Pareto": ""}


def plot_cost(data, name, save_path, i, N, W, K, R, COMR):
    fig, ax = plt.subplots()
    low_error = data[:, 1]-data[:, 0]
    sup_error = data[:, 2]-data[:, 1]
    plt.errorbar(range(data.shape[0]), data[:, 1], [low_error, sup_error],
                 fmt='-', capsize=1.0, elinewidth=0.5)
    ax.set_xlabel('Time (iteration)')
    ax.set_ylabel('Idleness term value')
    plt.savefig(save_path+'/graph_'+name+'_cost_N_'+str(N)+'_W_'+str(W)+'_K_'
                + str(K)+'_R_'+str(R)+'_COMR_'+str(COMR)+'.pdf')
    plt.close()


def plot_res(data, methods, name, root_path, i, N, W, K, R, COMR, I,
             labels, markers, colors, y_legend):
    fig, ax = plt.subplots()
    for method in methods:
        T = len(data[method])
        ax.plot(range(T), data[method]/I,
                label=labels[method], lw=.30, marker=markers[method],
                markersize=.50, color=colors[method])

    ax.legend(loc='upper left', shadow=True)
    ax.set_xlabel('Time (iteration)')
    ax.set_ylabel(y_legend)
    plt.savefig(root_path+'/graph_'+name+'_N_'+str(N)+'_W_'+str(W)+'_K_'
                + str(K)+'_R_'+str(R)+'_COMR_'+str(COMR)+'.pdf')
    for method in methods:
        method_mean = data[method].mean()
        method_std = 2*(data[method]/I).std()/(T**0.5)
        y_data = np.full(T, method_mean/I)
        ax.plot(range(T), y_data, lw=.80, color=colors[method])
        plt.fill_between(range(T),
                         y_data-method_std,
                         y_data+method_std,
                         alpha=.2, color=colors[method])
    plt.savefig(root_path+'/graph_'+name+'_N_'+str(N)+'_W_'+str(W)+'_K_'
                + str(K)+'_R_'+str(R)+'_COMR_'+str(COMR)
                + '_with_mean_and_std.pdf')


def fill_dics(mean_dic, max_dic, ener_dic, comp_dic, root_path, methods, I,
              i, N, W, K, R, COMR):
    for method in methods:
        for i in range(I):
            print(method, N, R, i)
            df = pd.read_csv(root_path+'/inst_'+str(i)+'/'+method
                             + '/dataframe_N_'+str(N)+'_W_'+str(W)+'_K_'
                             + str(K)+'_R_'+str(R)+'_COMR_'+str(COMR)+'_'
                             + str(i)+method+'.csv')
            mean_dic[method] += df.mean_idleness
            max_dic[method] += df.max_idleness.values
            criteria = ['nergy' in ind for ind in df.columns]
            ener_dic[method] += df.loc[:, criteria].mean(axis=1)
            comp_dic[method] += df.nb_components
            dfc = df.copy().replace(0, np.nan)
            idle_data = np.dstack((dfc.filter(regex='idle_term').min(axis=1),
                                   dfc.filter(regex='idle_term').mean(axis=1),
                                   dfc.filter(regex='idle_term').max(axis=1)))[0]
            enerc_data = np.dstack((dfc.filter(regex='ener_term').min(axis=1),
                                    dfc.filter(regex='ener_term').mean(axis=1),
                                    dfc.filter(regex='ener_term').max(axis=1)))[0]
            coms_data = np.dstack((dfc.filter(regex='coms_term').min(axis=1),
                                   dfc.filter(regex='coms_term').mean(axis=1),
                                   dfc.filter(regex='coms_term').max(axis=1)))[0]
            save_path = (root_path+'/inst_'+str(i)+'/'+method)
            plot_cost(idle_data, 'idle', save_path, i, N, W, K, R, COMR)
            plot_cost(enerc_data, 'ener', save_path, i, N, W, K, R, COMR)
            plot_cost(coms_data, 'coms', save_path, i, N, W, K, R, COMR)


# Gather data from all dfs
for N, W in [(5, 5), (10, 10)]:  # ((50, 10), (100, 25)):#
    for K in [4]:
        for R in (3, 5, 10):
            if R < W and R >= N/10:
                for LOSS in [0.0]:
                    for COMR in [1.]:
                        print("\n\n\n\n****************************\n\n\n\n"
                              + 'Starting instances with N = ' + str(N)
                              + ',W = ' + str(W) + ', R = ' + str(R) + '.'
                              + "\n\n\n\n****************************\n\n\n\n")
                        # mean idleness
                        mean_dic = {method: np.zeros(T) for method in methods}
                        max_dic = {method: np.zeros(T) for method in methods}
                        ener_dic = {method: np.zeros(T) for method in methods}
                        comp_dic = {method: np.zeros(T) for method in methods}
                        root_path = ("./instances_test_pareto_no_ener/n_"
                                     + str(N)+'_W_'+str(W)+'_K_'
                                     + str(K)+'_R_'+str(R))
                        print('Reading data.')
                        fill_dics(mean_dic, max_dic, ener_dic,
                                  comp_dic, root_path, methods, I,
                                  i, N, W, K, R, COMR)

                        plot_res(mean_dic, methods, 'mean_idle', root_path, i,
                                 N, W, K, R, COMR, I, labels, markers, colors,
                                 'Mean Idleness (iteration)')

                        plot_res(max_dic, methods, 'max_idle', root_path, i,
                                 N, W, K, R, COMR, I, labels, markers, colors,
                                 'Max Idleness (iteration)')

                        plot_res(ener_dic, methods, 'ener', root_path, i,
                                 N, W, K, R, COMR, I, labels, markers, colors,
                                 'Remaining energy')

                        plot_res(comp_dic, methods, 'nb_cc', root_path, i,
                                 N, W, K, R, COMR, I, labels, markers, colors,
                                 'Number of connected components')
