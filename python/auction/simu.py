from space_graph import spaceGraph
from obstacleCollection import obstacleCollection
import networkx as nx
import numpy as np
import pandas as pd
import read_graph as rg
from pathlib import Path
import matplotlib.pyplot as plt


def create_instance(N, W, K, R, P, i, dad_path, nb_obst, graph_type='random'):
    begin_pos = np.random.choice(N, R)
    G = spaceGraph(N, R, W, graph_type=graph_type)
    print(G.waypoints)
    OBST = obstacleCollection(int(nb_obst), 0.1, 0.5)
    OBST.buildObs(G)
    Path(dad_path).mkdir(parents=True, exist_ok=True)
    nx.write_adjlist(G.graph, dad_path + '/random_graph_N_'+str(N)+'_W_'+str(W)
                     + '_K_'+str(K)+'_R_'+str(R)+'_'+str(i)+'.txt')
    plot = G.plot(dad_path, save=False, close=False)
    plot = OBST.plot(dad_path, plot, save=False, close=False)
    plot.savefig(dad_path + '/space_graph_N_'+str(N)+'_W_'+str(W)+'_K_'+str(K)
                 + '_R_'+str(R)+'_'+str(i)+'.pdf')
    plot.close()
    OBST.totxt(dad_path + '/obstacles_N_'+str(N)+'_W_'+str(W)+'_K_'+str(K)
               + '_R_'+str(R)+'_'+str(i)+'.txt')
    file1 = open(dad_path + '/weights_N_'+str(N)+'_W_'+str(W)+'_K_'+str(K)
                 + '_R_'+str(R)+'_'+str(i)+'.txt', "w")
    file1.writelines(str(G.weights))
    file1.close()
    positions = [list(G.graph.nodes[i]['pos']) for i in range(N)]
    file2 = open(dad_path + '/positions_N_'+str(N)+'_W_'+str(W)+'_K_'+str(K)
                 + '_R_'+str(R)+'_'+str(i)+'.txt', "w")
    file2.writelines(str(positions).replace('[', '').replace(']', ''))
    file2.close()
    return G, begin_pos, OBST


def read_instance(N, W, K, R, P, LOSS, COMR, i, dad_path):
    READ_PATH = "instances_new_bid/n_"+str(N)+'_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+"/inst_"+str(i)+"/"
    waypoints = rg.read_waypoints(READ_PATH+"IDLENESS/", N, W, K, R,
                                  COMR, i, LOSS)
    graph = rg.read_graph(READ_PATH, N, W, K, R, i)
    weights = rg.read_weights(READ_PATH, N, W, K, R, i)
    positions = rg.read_positions(READ_PATH, N, W, K, R, i)
    G = spaceGraph(waypoints, graph_type='read', size=(N, K, P), pos=positions,
                   weights=weights, input_graph=graph)
    begin_pos = rg.read_begin_pos(READ_PATH+"IDLENESS/", graph, N,
                                  W, K, R, COMR, i, LOSS)
    return G, begin_pos


def simu_compute_auctions(robots, t, A, G, C, OBST, method, dic_data):
    for robot in robots:
        if robot.destination is None and t % A == 0:
            if robot.energy > 0:
                tic, tec, tcc = robot.compute_auctions(t, G, C, robots,
                                                       OBST.collection, method)
                for key in G.waypoints:
                    dic_data['idle_term_r'+str(robot.ind)+'node_'
                             + str(key)].append(tic.get(key, 0))
                    dic_data['ener_term_r'+str(robot.ind)+'node_'
                             + str(key)].append(tec.get(key, 0))
                    dic_data['coms_term_r'+str(robot.ind)+'node_'
                             + str(key)].append(tcc.get(key, 0))
            else:
                for key in G.waypoints:
                    dic_data['idle_term_r'+str(robot.ind)+'node_'
                             + str(key)].append(0)
                    dic_data['ener_term_r'+str(robot.ind)+'node_'
                             + str(key)].append(0)
                    dic_data['coms_term_r'+str(robot.ind)+'node_'
                             + str(key)].append(0)
        else:
            for key in G.waypoints:
                dic_data['idle_term_r'+str(robot.ind)+'node_'
                         + str(key)].append(0)
                dic_data['ener_term_r'+str(robot.ind)+'node_'
                         + str(key)].append(0)
                dic_data['coms_term_r'+str(robot.ind)+'node_'
                         + str(key)].append(0)


def simu_share_info(robots, LOSS, t, G):
    for robot in robots:
        if robot.energy > 0:
            for suscripter in robot.com_suscripters:
                suscripter.receive_auctions(robot.auctions, LOSS)
                suscripter.receive_destination(robot.team_dest, LOSS)
                suscripter.receive_last_visits(robot.last_visits, LOSS)
                suscripter.receive_pos(robot.team_pos,
                                       robot.team_node, LOSS)
                suscripter.receive_paths(robot.team_paths, LOSS)
                suscripter.receive_costs(robot.team_costs, LOSS)
                robot.receive_auctions(suscripter.auctions, LOSS)
                robot.receive_destination(suscripter.team_dest, LOSS)
                robot.receive_last_visits(suscripter.last_visits, LOSS)
                robot.receive_pos(suscripter.team_pos,
                                  suscripter.team_node, LOSS)
                robot.receive_paths(suscripter.team_paths, LOSS)
                robot.receive_costs(suscripter.team_costs, LOSS)
                robot.update_team_dest(t)
                suscripter.update_team_dest(t)
                robot.update_team_pos_and_path(G, t)
                suscripter.update_team_pos_and_path(G, t)


def simu_move_robots(robots, G, t, A, method):
    for robot in robots:
        if robot.destination is None:
            if robot.energy > 0:
                if "Pareto" in method:
                    robot.pareto_alloc(t, G, top_rank=0,
                                       assigned_ws=[], assigned_rs=[])
                else:
                    robot.task_alloc(G, t, A)
        elif robot.destination is not None:
            if robot.energy > 0:
                robot.move(G, t)
        robot.energy = max([0, robot.energy-robot.standing_cons])


def verbose_iteration(verbose, t, G, robots):
    if verbose == 'FULL' or verbose == 'IDLENESS':
        print('At time ', t, ' :')
        for waypoint in G.waypoints:
            print('Waypoint ', waypoint, 'has idleness = ',
                  t-G.last_visits[waypoint])
    if verbose == 'FULL' or verbose == 'MOTION':
        print('At time ', t, ' : \n')
        for ind, r in enumerate(robots):
            print('Robot ', ind, ' is in position : ', r.node, r.pos,
                  ', and goes to position : ', r.destination,
                  'through path : ',
                  r.path, 'it has energy', r.energy, '\n',
                  'Robot ', ind, ' has placed following auctions : ',
                  r.auctions, '\n')
        print('\n\n\n____________________________________\n\n')


def save_data_iteration(dic_data, max_idle, mean_idle, mean_ener_dic, t, W,
                        robots, C, R, G, nb_robot_bidding, method):
    dic_data['max_idleness'].append(max_idle)
    dic_data['mean_idleness'].append(mean_idle/W)
    sum_ener = 0
    for ind, r in enumerate(robots):
        dic_data['r'+str(ind)+'_pos'].append(r.pos)
        dic_data['r'+str(ind)+'_anergy'].append(r.energy)
        dic_data['r'+str(ind)+'_auctions'].append(r.auctions)
        dic_data['r'+str(ind)+'_path'].append(r.path)
        dic_data['r'+str(ind)+'_destination'].append(r.destination)
        if not r.auctions == {}:
            nb_robot_bidding[t] += 1
        sum_ener += r.energy
    nb_cc = nx.number_connected_components(C.graph)
    dic_data['nb_components'].append(nb_cc)
    mean_ener_dic[''.join(method)][t] += sum_ener/R
    for w in G.waypoints:
        dic_data['node_'+str(w)+'_idleness'].append(t-G.last_visits[w])
    dic_data['last_visits'].append({w: G.last_visits[w] for w in G.waypoints})


def save_data_instance(R, G, dad_path, method, dic_data, N, W, K, COMR, i):
    columns = (['max_idleness', 'mean_idleness']
               + ['r'+str(r)+'_pos' for r in range(R)]
               + ['node_'+str(w)+'_idleness' for w in G.waypoints]
               + ['r'+str(r)+'_anergy' for r in range(R)]
               + ['r'+str(r)+'_auctions' for r in range(R)]
               + ['r'+str(r)+'_path' for r in range(R)]
               + ['r'+str(r)+'_destination' for r in range(R)]
               + ['idle_term_r'+str(r)+'node_'+str(w)
                  for r in range(R) for w in G.waypoints]
               + ['ener_term_r'+str(r)+'node_'+str(w)
                  for r in range(R) for w in G.waypoints]
               + ['coms_term_r'+str(r)+'node_'+str(w)
                  for r in range(R) for w in G.waypoints]
               + ['last_visits', 'nb_components'])

    son_path = dad_path + "/" + ''.join(method)
    db = pd.DataFrame(data=dic_data, columns=columns)
    db.to_csv(son_path + '/dataframe_N_'+str(N)+'_W_'+str(W)+'_K_'+str(K)+'_R_'
              + str(R)+'_COMR_'+str(COMR)+'_'+str(i)+''.join(method)+'.csv')
    return db


def plot_idleness(db, dic_data, nb_robot_bidding,
                  R, N, W, K, COMR, i, method, dad_path):
    fig, ax = plt.subplots()
    ax.plot(db.index, dic_data['mean_idleness'], label='Mean idleness',
            lw=.30, color='b')
    ax.plot(db.index, dic_data['max_idleness'], label='Max idleness',
            lw=.30, color='r')

    ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis

    ax2.set_ylabel('Number of robots bidding', color='k')
    ax2.bar(db.index, nb_robot_bidding, color='k', label='Bidding robots')
    plt.ylim(0, R*5)

    ax.legend(loc='upper left', shadow=True)
    ax.set_xlabel('Time (iteration)')
    ax.set_ylabel('Idleness (iteration)')
    plt.grid(True)
    fig.tight_layout()
    son_path = dad_path + "/" + ''.join(method)
    plt.savefig(son_path+'/graph_mean_idleness_N_'+str(N)
                + '_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+'_COMR_'
                + str(COMR)+'_'+str(i)+''.join(method)+'.png',
                dpi=300)
    plt.close()


def plot_nb_comp(db, dic_data, T, R, N, W, K, COMR, i, method, dad_path, col):
    fig, ax = plt.subplots()
    ax.fill_between(db.index, 0, dic_data['nb_components'],
                    label='Number of connected composants',
                    lw=.50, color=col[''.join(method)])

    ax.plot(db.index, np.full(T, np.mean(dic_data['nb_components'])),
            label='Number of connected composants', lw=.50, color='k')
    ax.legend(loc='upper left', shadow=True)
    ax.set_xlabel('Time (iteration)')
    ax.set_ylabel('Number of connected composants')
    plt.grid(True)
    fig.tight_layout()
    son_path = dad_path + "/" + ''.join(method)
    plt.savefig(son_path+'/graph_connected_components_N_'+str(N)
                + '_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+'_COMR_'+str(COMR)
                + '_'+str(i)+''.join(method)+'.png',
                dpi=300)
    plt.close()


def plot_ener(methods, db, mean_ener_dic, NBINST, dad_path,
              N, W, K, R, COMR, i, col, labels):
    fig, ax = plt.subplots()
    for method in methods:
        ax.plot(db.index, mean_ener_dic[''.join(method)]/NBINST,
                label=labels[''.join(method)],
                lw=.30, color=col[''.join(method)])

    ax.legend(loc='lower left', shadow=True)
    ax.set_xlabel('Time (iteration)')
    ax.set_ylabel('Remaining Energy')
    plt.grid(True)
    plt.savefig(dad_path+'/graph_energy_N_'+str(N)
                + '_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+'_COMR_'
                + str(COMR)+'_'+str(i)+'.png',
                dpi=300)
    plt.close()
