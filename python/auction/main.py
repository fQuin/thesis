from robot import Robot
from com_graph import comGraph
import time
import numpy as np
import simu as sim
from pathlib import Path
import imageio

# Set parameters
P = 0.5  # probability of creating a shortcut
T = 400  # number of time periods
A = 5  # auction delay
# Video parameters
video_on = False

# auction computation method. List of string IDLENESS, ENERGY, COMS
methods = [["IDLENESS", "ENERGY", "COMS", "MUL"],
           ["IDLENESS"],
           ["IDLENESS", "ENERGY"],
           ["IDLENESS", "ENERGY", "COMS"],
           ["IDLENESS", "ENERGY", "COMS", "MIN"],
           ["Pareto"]]
methods_name = ["IDLENESS", "IDLENESSENERGY",
                "IDLENESSENERGYCOMSMUL", "IDLENESSENERGYCOMS",
                "IDLENESSENERGYCOMSMIN", "Pareto"]
labels = {"IDLENESS": "Idleness",
          "IDLENESSENERGY": "Idleness and Energy",
          "IDLENESSENERGYCOMS": "Idleness and Energy and Coms",
          "IDLENESSENERGYNORM": "Idleness and Energy Normalized",
          "IDLENESSENERGYCOMSNORM": "Idleness and Energy Normalized, Coms not Normalized",
          "IDLENESSENERGYCOMSNORMALL": "Idleness and Energy and Coms Normalized",
          "IDLENESSENERGYCOMSNORMMUL": "Idleness and Energy Normalized multplied by Coms",
          "IDLENESSENERGYCOMSMUL": "Idleness and Energy multplied by Coms",
          "IDLENESSENERGYCOMSMIN": "Reversed coms",
          "Pareto": "Pareto"}
colors = {"IDLENESS": "#1f77b4",
          "IDLENESSENERGY": "#ff7f0e", "IDLENESSENERGYCOMS": "#2ca02c",
          "IDLENESSENERGYNORM": "#d62728",
          "IDLENESSENERGYCOMSNORM": "#9467bd",
          "IDLENESSENERGYCOMSNORMALL": "gold",
          "IDLENESSENERGYCOMSNORMMUL": "pink",
          "IDLENESSENERGYCOMSMUL": "cyan",
          "IDLENESSENERGYCOMSMIN": "r",
          "Pareto": "k"}
markers = {"IDLENESS": "",
           "IDLENESSENERGY": "",
           "IDLENESSENERGYCOMS": "",
           "IDLENESSENERGYCOMSNORMMUL": "",
           "IDLENESSENERGYNORM": "",
           "IDLENESSENERGYCOMSNORM": "",
           "IDLENESSENERGYCOMSNORMALL": "",
           "IDLENESSENERGYCOMSMUL": "",
           "IDLENESSENERGYCOMSMIN": "",
           "Pareto": ""}
VERBOSE = "NONE"
READ = False
COMR = 0.4
LOSS = 0
NBINST = 10  # nombre d'instances pour chaque jeu de paramètres


for N, W in [(5, 5), (10, 10)]:  # [(10,5)]:
    for K in [4]:
        for R in [3, 5, 10]:
            if R < W and R >= N/10:
                for LOSS in [0.0]:
                    print('Starting instances with N=', N,
                          ' W=', W, ' K=', K, ' R=', R)
                    NB_OBST = int(N/10)
                    for i in range(NBINST):
                        mean_ener_dic = {methods_name[name]: []
                                         for name in range(len(methods))}
                        grandpa_path = "./instances_test_pareto_sum_costs/n_"+str(N)+'_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)  # +'_COMR_'+str(COMR)
                        Path(grandpa_path).mkdir(parents=True, exist_ok=True)

                        dad_path = grandpa_path + '/inst_' + str(i)
                        if not READ:
                            G, begin_pos, OBST = sim.create_instance(
                                N, W, K, R, P, i, dad_path, NB_OBST,
                                graph_type='longiligne')
                        else:
                            G, begin_pos = sim.read_instance(
                                N, W, K, R, P, LOSS, COMR, i, dad_path)

                        weights = G.weights
                        mean_weight = np.array([weights[k] for k in weights]).mean()
                        for method in methods:
                            son_path = dad_path + "/" + ''.join(method)
                            Path(son_path).mkdir(parents=True, exist_ok=True)
                            Path(son_path+"/frames").mkdir(parents=True,
                                                           exist_ok=True)
                            for COMR in [1.]:
                                G.last_visits = {w: 0 for w in G.waypoints}
                                robots = []
                                for r in range(R):
                                    robots.append(Robot(r, int(begin_pos[r]),
                                                        G, com_range=COMR,
                                                        velocity=mean_weight/10))

                                mean_ener_dic[''.join(method)] = np.zeros(T)
                                frames = []

                                dic_data = {'max_idleness': [],
                                            'mean_idleness': []}
                                for r in range(R):
                                    dic_data['r'+str(r)+'_pos'] = []
                                    dic_data['r'+str(r)+'_anergy'] = []
                                    dic_data['r'+str(r)+'_auctions'] = []
                                    dic_data['r'+str(r)+'_path'] = []
                                    dic_data['r'+str(r)+'_destination'] = []
                                    for w in G.waypoints:
                                        dic_data['idle_term_r'+str(r)+'node_'+str(w)] = []
                                        dic_data['ener_term_r'+str(r)+'node_'+str(w)] = []
                                        dic_data['coms_term_r'+str(r)+'node_'+str(w)] = []
                                dic_data['nb_components'] = []
                                for w in G.waypoints:
                                    dic_data['node_'+str(w)+'_idleness'] = []
                                dic_data['last_visits'] = []

                                nb_robot_bidding = [0]*T

                                sim_timer = 0

                                for t in range(T):
                                    if video_on:
                                        plot = G.plot(dad_path, save=False,
                                                      close=False)
                                        plot = OBST.plot(dad_path, plot,
                                                         save=False, close=False)
                                    mean_idle = 0
                                    max_idle = 0
                                    iter_start = time.time()
                                    for robot in robots:
                                        if robot.energy > 0:
                                            robot.update_com_suscripters(robots)
                                    C = comGraph(R)
                                    C.buildFromRobots(robots,
                                                      {r.ind: r.pos for r in robots},
                                                      OBST.collection)
                                    sim.simu_compute_auctions(
                                        robots, t, A, G, C,
                                        OBST, method, dic_data)

                                    sim.simu_share_info(robots, LOSS, t, G)

                                    for waypoint in G.waypoints:
                                        max_idle = max(max_idle,
                                                       t-G.last_visits[waypoint])
                                        mean_idle += t-G.last_visits[waypoint]

                                    sim.verbose_iteration(VERBOSE, t,
                                                          G, robots)

                                    sim.simu_move_robots(robots, G, t,
                                                         A, method)

                                    sim.save_data_iteration(dic_data, max_idle,
                                                            mean_idle,
                                                            mean_ener_dic, t,
                                                            W, robots, C, R, G,
                                                            nb_robot_bidding,
                                                            method)
                                iter_end = time.time()
                                sim_timer += iter_end - iter_start
                                if video_on:
                                    images = []
                                    for t in range(T):
                                        images.append(imageio.imread(son_path+"/frames/"+str(t)+".png"))
                                    imageio.mimsave(son_path+'/movie.gif',
                                                    images)

                                db = sim.save_data_instance(
                                    R, G, dad_path, method,
                                    dic_data, N, W, K, COMR, i)

                                print('Instance n°', i, ' solved with '
                                      + ''.join(method) + "ComR = "
                                      + str(COMR) + ': computing time = ',
                                      sim_timer, ' sec.')
                                print('Final mean and max idleness : ',
                                      mean_idle/W, max_idle)
                                file1 = open(son_path + '/results_N_'+str(N)
                                             + '_W_'+str(W)+'_K_'+str(K)+'_R_'
                                             + str(R)+'_LOSS_'+str(LOSS)+'_'
                                             + str(i)+'.txt', "a+")
                                L = ["Computing_time=" + str(sim_timer)]
                                file1.writelines(L)
                                file1.close()

                                sim.plot_idleness(
                                    db, dic_data, nb_robot_bidding, R, N, W, K,
                                    COMR, i, method, dad_path)

                                sim.plot_nb_comp(
                                    db, dic_data, T, R, N, W, K, COMR, i,
                                    method, dad_path, colors)
                        print('\n\n______________________________________\n\n')

                    sim.plot_ener(
                        methods, db, mean_ener_dic, NBINST, dad_path, N, W, K,
                        R, COMR, i, colors, labels)
