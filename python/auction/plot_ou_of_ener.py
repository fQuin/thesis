import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


# Gather data from all dfs
run_mean = {'IDLENESS': [], 'IDLENESSENERGY': [], 'IDLENESSENERGYCOMSNORM': [],
            'IDLENESSENERGYCOMS': [], 'IDLENESSENERGYNORM': []}
fig2, ax2 = plt.subplots()
for N, W in ((10, 5), (50, 10), (50, 25), (100, 10), (100, 25)):
    print(N, W)
    for K in (4, 6):
        for R in (3, 5, 10):
            if R < W:
                for LOSS in np.round(np.linspace(0, 0.8, 9), 3):

                    for i in range(100):
                        df = pd.read_csv("./instances/n_"+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'/inst_' + str(i)
                                         + '/dataframe_N_'+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'_'+str(i)+'IDLENESS'
                                         + '.csv')
                        for robot in range(R):
                            run_r_df = df[df['r'+str(robot)
                                             + '_energy'].lt(0.0001)]
                            if len(run_r_df.index) > 0:
                                run_r = df[df['r'+str(robot)
                                              + '_energy'].lt(0.0001)].index[0]
                                run_mean['IDLENESS'].append(run_r)
                            else:
                                run_mean['IDLENESS'].append(400)

                    for i in range(100):
                        df = pd.read_csv("./instances/n_"+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'/inst_' + str(i)
                                         + '/dataframe_N_'+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'_'+str(i)
                                         + 'IDLENESSENERGY'+'.csv')
                        run_max = 0
                        for robot in range(R):
                            run_r_df = df[df['r'+str(robot)
                                             + '_energy'].lt(0.0001)]
                            if len(run_r_df.index) > 0:
                                run_r = df[df['r'+str(robot)
                                              + '_energy'].lt(0.0001)].index[0]
                                run_mean['IDLENESSENERGY'].append(run_r)
                            else:
                                run_mean['IDLENESSENERGY'].append(400)

                    for i in range(100):
                        df = pd.read_csv("./instances/n_"+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'/inst_' + str(i)
                                         + '/dataframe_N_'+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'_'+str(i)
                                         + 'IDLENESSENERGYCOMSNORM'+'.csv')
                        run_max = 0
                        for robot in range(R):
                            run_r_df = df[df['r'+str(robot)
                                             + '_energy'].lt(0.0001)]
                            if len(run_r_df.index) > 0:
                                run_r = df[df['r'+str(robot)
                                              + '_energy'].lt(0.0001)].index[0]
                                run_mean['IDLENESSENERGYCOMSNORM'].append(run_r)
                            else:
                                run_mean['IDLENESSENERGYCOMSNORM'].append(400)

                    for i in range(100):
                        df = pd.read_csv("./instances/n_"+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'/inst_' + str(i)
                                         + '/dataframe_N_'+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'_'+str(i)
                                         + 'IDLENESSENERGYCOMS'+'.csv')
                        run_max = 0
                        for robot in range(R):
                            run_r_df = df[df['r'+str(robot)
                                             + '_energy'].lt(0.0001)]
                            if len(run_r_df.index) > 0:
                                run_r = df[df['r'+str(robot)
                                              + '_energy'].lt(0.0001)].index[0]
                                run_mean['IDLENESSENERGYCOMS'].append(run_r)
                            else:
                                run_mean['IDLENESSENERGYCOMS'].append(400)

                    for i in range(100):
                        df = pd.read_csv("./instances/n_"+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'/inst_' + str(i)
                                         + '/dataframe_N_'+str(N)+'_W_'+str(W)
                                         + '_K_'+str(K)+'_R_'+str(R)+'_LOSS_'
                                         + str(LOSS)+'_'+str(i)
                                         + 'IDLENESSENERGYNORM'+'.csv')
                        run_max = 0
                        for robot in range(R):
                            run_r_df = df[df['r'+str(robot)
                                             + '_energy'].lt(0.0001)]
                            if len(run_r_df.index) > 0:
                                run_r = df[df['r'+str(robot)
                                              + '_energy'].lt(0.0001)].index[0]
                                run_mean['IDLENESSENERGYNORM'].append(run_r)
                            else:
                                run_mean['IDLENESSENERGYNORM'].append(400)

to_plot = []
for key in run_mean:
    to_plot.append(np.mean(run_mean[key]))
for i in range(len(to_plot)):
    ax2.bar(i, to_plot[i], label=list(run_mean.keys())[i])
ax2.legend(loc='lower right', shadow=False)
plt.savefig("./instances/graph_energy.png", dpi=1e3)
plt.close()
