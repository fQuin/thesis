import numpy as np
import networkx as nx
from math import isclose
from networkx import grid_graph
import matplotlib.pyplot as plt
from geometry import compute_dist


class spaceGraph:
    def __init__(self, N, R, W, graph_type='random', LD=2/3,
                 pos=None, waypoints=None, weights=None, input_graph=None):
        # LD = Layer Decay : proportion of nodes that survive in upper layer
        if graph_type == 'grid':
            graph = grid_graph([N, N])
        elif graph_type == 'maze_graph':
            graph = nx.sedgewick_maze_graph()
        elif graph_type == 'read':
            graph = input_graph
        elif graph_type == 'longiligne':
            N = N
            pos = self.init_pos_longiligne(N, LD)
            edges = self.init_edges_longiligne(N, LD, pos)
            graph = nx.Graph()
            for n in range(pos.shape[0]):
                graph.add_node(n)
            for edge in edges:
                graph.add_edge(*edge)
            if waypoints is None:
                waypoints = list(np.random.choice(pos.shape[0],
                                                  W, replace=False))
        else:
            graph = nx.connected_watts_strogatz_graph(N, 4, .5,
                                                      tries=int(1e6))
            pos = np.random.uniform(-1, 1, (N, 2))
        if waypoints is None:
            waypoints = list(np.random.choice(N, W, replace=False))
        if weights is None:
            weights = {}
            for edge in graph.edges:
                weights[(edge[0], edge[1])] = compute_dist(pos[edge[0]],
                                                           pos[edge[1]])
        graph.add_weighted_edges_from([(edge[0], edge[1],
                                        weights[tuple(sorted((edge[0],
                                                              edge[1])))])
                                       for edge in graph.edges])
        nx.set_node_attributes(graph,
                               {node: pos[node]
                                for node in range(pos.shape[0])}, 'pos')
        self.weights = weights
        self.min_weight = min(weights.values())
        self.graph = graph
        self.pos_2D = pos
        self.waypoints = waypoints
        self.last_visits = {w: 0 for w in waypoints}

    def print_waypoints(self):
        print("Waypoint nodes : ", self.waypoints)

    def plot_graph(self):
        pos = nx.spring_layout(self.graph)
        nx.draw_networkx_nodes(self.graph, pos,
                               nodelist=list(self.graph.nodes),
                               node_color='b',
                               node_size=500)
        nx.draw_networkx_nodes(self.graph, pos,
                               nodelist=self.waypoints,
                               node_color='r',
                               node_size=500)
        nx.draw_networkx_edges(self.graph, pos, width=1.0)
        nx.draw_networkx_labels(self.graph, pos, font_size=16)
        plt.axis('off')
        plt.show()

    def path_length(self, path, weight='weight'):
        return sum([self.graph.edges[(path[i], path[i+1])][weight]
                    for i in range(len(path)-1)])

    def dist_2D(self, x, y):
        return ((self.pos_2D[x][0]-self.pos_2D[y][0])**2
                + (self.pos_2D[x][1]-self.pos_2D[y][1])**2)**0.5

    def init_pos_longiligne(self, N, LD):
        root_x = np.random.uniform(.25, 1., N-1)
        root_pos = np.squeeze(
            np.dstack((np.hstack(([0], np.cumsum(root_x))),
                       np.zeros(N))))
        layer_1_ind = np.random.choice(range(N), int(N*LD), replace=False)
        layer_1_ind.sort()
        layer_1_pos = np.squeeze(
            root_pos[layer_1_ind]
            + np.dstack((np.random.uniform(-1e-5, 1e-5, layer_1_ind.shape),
                         np.random.uniform(.5, .75, layer_1_ind.shape))))
        pos = np.vstack((root_pos, layer_1_pos))
        layer_minus_1_ind = np.random.choice(range(N), int(N*LD),
                                             replace=False)
        layer_minus_1_ind.sort()
        layer_minus_1_pos = np.squeeze(
            root_pos[layer_minus_1_ind]
            + np.dstack((np.random.uniform(-1e-5, 1e-5,
                                           layer_minus_1_ind.shape),
                         np.random.uniform(-.75, -.5,
                                           layer_minus_1_ind.shape))))
        pos = np.vstack((pos, layer_minus_1_pos))
        layer_2_ind = np.random.choice(range(int(N*LD)), int(N*LD**2),
                                       replace=False)
        layer_2_ind.sort()
        layer_2_pos = np.squeeze(
            layer_1_pos[layer_2_ind]
            + np.dstack((np.random.uniform(-1e-5, 1e-5, layer_2_ind.shape),
                         np.random.uniform(.25, .5, layer_2_ind.shape))))
        pos = np.vstack((pos, layer_2_pos))
        layer_minus_2_ind = np.random.choice(range(int(N*LD)), int(N*LD**2),
                                             replace=False)
        layer_minus_2_ind.sort()
        layer_minus_2_pos = np.squeeze(
            layer_minus_1_pos[layer_minus_2_ind]
            + np.dstack((np.random.uniform(-1e-5, 1e-5,
                                           layer_minus_2_ind.shape),
                         - np.random.uniform(.25, .5,
                                             layer_minus_2_ind.shape))))
        pos = np.vstack((pos, layer_minus_2_pos))
        return pos

    def init_edges_longiligne(self, N, LD, pos):
        edges = []
        for ind1, (x1, y1) in enumerate(pos):
            for ind2, (x2, y2) in enumerate(pos[ind1+1:]):
                ind2 += ind1+1
                if ind1 == ind2-1 and ind2 < N:
                    edges.append((ind1, ind2))
                if ind1 < N and ind2 < N+2*int(N*LD) and ind2 >= N and isclose(x1, x2, abs_tol=1e-4):
                    edges.append((ind1, ind2))
                if ind1 < N+2*int(N*LD) and ind1 >= N and y1*y2 > 0 and isclose(x1, x2, abs_tol=1e-4):
                    edges.append((ind1, ind2))
                if ind1 >= N and ind1 == ind2-1 and y1*y2 > 0 and np.random.uniform(0, 1)<0.5:
                    edges.append((ind1, ind2))
        return edges

    def plot(self, path, save=True, close=True, print_label=True, grid=True,
             n_color='blue', n_size=700, w_color='red', e_width=3):
        elarge = [(u, v) for (u, v, d) in self.graph.edges(data=True)]
        pos = nx.get_node_attributes(self.graph, 'pos')
        # nodes
        color_map = []
        for node in self.graph:
            if node in self.waypoints:
                color_map.append(w_color)
            else:
                color_map.append(n_color)
        nx.draw_networkx_nodes(self.graph, pos, node_size=n_size,
                               node_color=color_map, with_labels=True)

        # edges
        labels = nx.get_edge_attributes(self.graph, 'weight')
        for label in labels:
            labels[label] = round(labels[label], 3)
        nx.draw_networkx_edge_labels(self.graph, pos, edge_labels=labels)
        nx.draw_networkx_edges(self.graph, pos, edgelist=elarge,
                               e_width=3)

        # labels
        if print_label:
            nx.draw_networkx_labels(self.graph, pos, font_size=20)

        plt.grid(grid)
        plt.gca().set_aspect("equal")
        if save:
            plt.savefig(path + '/space_graph'+'.pdf')
        if close:
            plt.close()
        else:
            return plt


if __name__ == "__main__":
    G = spaceGraph([(2, 0), (0, 2), (2, 2), (0, 0)])
    G.plot_graph()
