import networkx as nx
import numpy as np
# from robot import Robot
# from space_graph import spaceGraph


def equation_droite(x, y):
    # trouve l'équation de la droite passant par deux points
    a = (x[1]-y[1])/(x[0]-y[0])
    b = y[1] - a*y[0]
    return (a, b)


class comGraph:
    def __init__(self, nb_robots):
        self.nb_robots = nb_robots
        self.graph = nx.empty_graph(nb_robots)

    def buildFromRobots(self, robots, dic_pos, obstacles):
        f_r_pos = dic_pos[next(iter(dic_pos))]
        mul = 1
        for r in range(len(robots)):
            if r not in dic_pos:
                dic_pos[r] = (f_r_pos[0] + mul*1e3, f_r_pos[1] + mul*1e3)
                mul += 1
        for r in range(len(robots)):
            if robots[r].energy > 0:
                for s in range(len(robots)):
                    if not r == s and robots[s].energy > 0:
                        if (((dic_pos[r][0]-dic_pos[s][0])**2
                             + (dic_pos[r][1]-dic_pos[s][1])**2)**0.5 <= min(
                                 robots[r].com_range, robots[s].com_range)
                                and not self.checkCommunication(dic_pos[r],
                                                                dic_pos[s],
                                                                obstacles)):
                            self.graph.add_edge(r, s)

    def checkCommunication(self, p1, p2, obstacles):
        if p1[0]-p2[0] == 0 and p1[1]-p2[1] == 0:
            return False
        a, b = equation_droite(p1, p2)
        for obstacle in obstacles:
            c, d = equation_droite(*obstacle)
            s = (d-b)/(a-c)
            if s >= min(p1[0], p2[0]) and s <= max(p1[0], p2[0]):
                return True
        return False


if __name__ == "__main__":
    R = 5
    # G = spaceGraph([], graph_type='read',
    # input_graph=nx.sedgewick_maze_graph())
    robots = []
    # for r in range(R):
    # robots.append(Robot(r, np.random.choice(5), G))
    comG = comGraph(R)
    comG.buildFromRobots(robots)
    for r in robots:
        print(r.ind, np.round(r.pos, 3))
    print(comG.graph.edges)
