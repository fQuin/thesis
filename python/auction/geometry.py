# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 15:09:44 2020

@author: fequi
"""


def compute_dist(x, y):
    return ((x[0]-y[0])**2+(x[1]-y[1])**2)**0.5


def equation_droite(x, y):
    # trouve l'équation de la droite passant par deux points
    a = (x[1]-y[1])/(x[0]-y[0])
    b = y[1] - a*y[0]
    return (a, b)


def find_intersect(pos, line, v):
    # trouve l'insection de la droite définie par son équation (line)
    # et le cercle de centre pos et de rayon v
    r_x, r_y = pos
    a, b = line
    carre = 1+a**2
    b_term = (2*r_x+2*a*b+2*a*r_y)
    cons = r_x**2+r_y**2+b**2+2*b*r_y-v
    if b_term**2-4*carre*cons > 0:
        x1 = (b_term+(b_term**2-4*carre*cons)**0.5)/2*carre
        x2 = (b_term-(b_term**2-4*carre*cons)**0.5)/2*carre
        return x1, x2
    elif b_term**2-4*carre*cons == 0:
        x1 = (b_term+(b_term**2-4*carre*cons)**0.5)/2*carre
        return x1
    else:
        print('HEEEELP')


def find_new_point(pos, line, v):
    # trouve les points correspondant à un déplacement de v distance
    # depuis le point pos sur la droite définie par l'équation (line)
    r_x, r_y = pos
    a, b = line
    # formule trouvée en résolvant ||pos-X||²=v². Il faut dévellopper et faire
    # le changement de variable t = (pos_x-X_x)²
    n_x = r_x-(v**2/(1+a**2))**0.5
    n_y = n_x*a+b
    p_x = r_x+(v**2/(1+a**2))**0.5
    p_y = p_x*a+b
    return (n_x, n_y), (p_x, p_y)
