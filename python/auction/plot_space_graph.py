from space_graph import spaceGraph
import networkx as nx
import numpy as np
import read_graph as rg
from pathlib import Path
import matplotlib.pyplot as plt


# Set parameters
# N = 10 #number of nodes
# K = 4 #number of nearest neightbors connected
P = 0.5  # probability of creating a shortcut
# W = 5 #number of waypoints
# R = 3 #number of robots
T = 400  # number of time periods
P = 5  # auction delay
methods = [["IDLENESS", "ENERGY", "NORM"]]
# auction computation method. List of string IDLENESS, ENERGY, COMS
VERBOSE = "NONE"
READ = True
NBINST = 100  # nombre d'instances pour chaque jeu de paramètres
for method in methods:
    for N, W in ((10, 5), (50, 10), (50, 25), (100, 10), (100, 25)):
        for K in (4, 6):
            for R in (3, 5, 10):
                if R < W:
                    for LOSS in np.round(np.linspace(0, 0.8, 9), 3):
                        print('Starting instances with N=', N, ' W=', W, ' K=',
                              K, ' R=', R, ' LOSS=', LOSS)
                        grandpa_path = "./instances/n_"+str(N)+'_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+'_LOSS_'+str(LOSS)
                        Path(grandpa_path).mkdir(parents=True, exist_ok=True)
                        for i in range(NBINST):
                            # Define a path graph
                            dad_path = grandpa_path + '/inst_' + str(i)

                            READ_PATH = "instances/n_"+str(N)+'_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+'_LOSS_'+str(LOSS)+"/inst_"+str(i)+"/"
                            waypoints = rg.read_waypoints(READ_PATH, N, W,
                                                          K, R, i, LOSS)
                            graph = rg.read_graph(READ_PATH, N, W, K, R, i)
                            weights = rg.read_weights(READ_PATH, N, W, K, R, i)
                            positions = rg.read_positions(READ_PATH, N,
                                                          W, K, R, i)
                            GRAPH = spaceGraph(waypoints, graph_type='read',
                                               size=(N, K, P), pos=positions,
                                               weights=weights,
                                               input_graph=graph)
                            begin_pos = rg.read_begin_pos(READ_PATH, graph, N,
                                                          W, K, R, i, LOSS)
                            G = GRAPH.graph
                            elarge = [(u, v) for (u, v, d)
                                      in G.edges(data=True)]

                            pos = nx.get_node_attributes(G, 'pos')
                            # positions for all nodes

                            # nodes
                            color_map = []
                            for node in G:
                                if node in waypoints:
                                    color_map.append('red')
                                else:
                                    color_map.append('blue')
                            nx.draw_networkx_nodes(G, pos, node_size=700,
                                                   node_color=color_map,
                                                   with_labels=True)

                            # edges
                            labels = nx.get_edge_attributes(G, 'weight')
                            for label in labels:
                                labels[label] = round(labels[label], 3)
                            nx.draw_networkx_edge_labels(G, pos,
                                                         edge_labels=labels)
                            nx.draw_networkx_edges(G, pos, edgelist=elarge,
                                                   width=3)

                            # labels
                            nx.draw_networkx_labels(G, pos, font_size=20)
                            plt.grid(True)
                            plt.gca().set_aspect("equal")
                            plt.savefig(dad_path + '/space_graph'+'.png',
                                        dpi=1e3)
                            plt.close()
