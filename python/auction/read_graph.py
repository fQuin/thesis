import pandas as pd
import networkx as nx
import numpy as np
import math


def read_waypoints(path, N, W, K, R, COMR, i, LOSS):
    # Récupération des waypoints
    full = path + "dataframe_" + "N_" + str(N) + '_W_' + str(W) + '_K_' + str(K) + '_R_' + str(R) + '_COMR_' + str(COMR) + '_' + str(i)  + "IDLENESS.csv"
    df = pd.read_csv(full)
    waypoints = []
    for col in df.columns:
        if 'node' in col and 'idleness' in col:
            waypoints.append(int(col[5:-9]))
    return waypoints


def read_graph(path, N, W, K, R, i):
    # Reconstitution du graph
    full = path + "random_graph_" + "N_" + str(N) + '_W_' + str(W) + '_K_' + str(K) + '_R_' + str(R) + '_' + str(i) + ".txt"
    graph = nx.read_adjlist(full, nodetype=int)
    return graph


def read_positions(path, N, W, K, R, i):
    # Reconstitution des poids
    full = path + "positions_" + "N_" + str(N) + '_W_' + str(W) + '_K_' + str(K) + '_R_' + str(R) + '_' + str(i) + ".txt"
    positions = np.loadtxt(full)
    return positions


def read_weights(path, N, W, K, R, i):
    # Reconstitution des poids
    full = path + "weights_" + "N_" + str(N) + '_W_' + str(W) + '_K_' + str(K) + '_R_' + str(R) + '_' + str(i) + ".txt"
    with open(full, "r") as data:
        weights = eval(data.read())
    return weights


def read_begin_pos(path, graph, N, W, K, R, COMR, i, LOSS):
    # Récupération des positions de départ des robots
    full = path + "dataframe_" + "N_" + str(N) + '_W_' + str(W) + '_K_' + str(K) + '_R_' + str(R) + '_COMR_' + str(COMR) + '_' + str(i) + "IDLENESS.csv"
    df = pd.read_csv(full)
    start_nodes = {}
    for r in range(R):
        r_pos = df['r'+str(r)+'_pos'][0][1:-1].split()
        r_pos = [float(r_pos[0]), float(r_pos[1])]
        for node in graph.nodes:
            node_pos = graph.nodes[node]['pos']
            if (math.isclose(node_pos[0], r_pos[0], rel_tol=1e-4)
                    and math.isclose(node_pos[1], r_pos[1], rel_tol=1e-4)):
                start_nodes[r] = int(node)
    return start_nodes


if __name__ == '__main__':
    # Set parameters
    N = 10  # number of nodes
    K = 4  # number of nearest neightbors connected
    P = 0.5  # probability of creating a shortcut
    W = 5  # number of waypoints
    R = 3  # number of robots
    T = 400  # number of time periods
    P = 5  # auction delay
    for i in range(100):
        i = 0
        PATH = "instances/n_"+str(N)+'_W_'+str(W)+'_K_'+str(K)+'_R_'+str(R)+"/inst_"+str(i)+"/"
        print(nx.circular_layout(read_graph(PATH)))
        print(read_begin_pos(PATH, read_graph(PATH)))
