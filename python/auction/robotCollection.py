from matplotlib.patches import Rectangle


class robotCollection:
    def __init__(self, robots):
        self.robots = robots

    def plot(self, path, plt=None, save=True, close=True,
             r_width=0.1, r_heigth=0.1, facecolor='g', alpha=0.8):
        currentAxis = plt.gca()
        for robot in self.robots:
            pos = (robot.pos[0]-r_width/2, robot.pos[1]-r_heigth/2)
            currentAxis.add_patch(Rectangle(pos, r_width, r_heigth,
                                  alpha=alpha, facecolor=facecolor, zorder=5))
        if save:
            plt.savefig(path)
        if close:
            plt.close()
        else:
            return plt
