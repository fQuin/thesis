from utils import remove_key
import numpy as np
import itertools


class funNode():

    def __init__(self, id, xs, pos, robot_poses):
        """Initialize the function node.
        :param id: ind, unique id of the function node (task's id).
        :param xs: list, the variables adjacent to the task.
        :param pos: 2D array, the cartesian position of the task.
        :param robot_poses: 2xR array, the cartesian position of the robots.
        """
        self.id = id
        self.qs = {}
        self.xs = xs
        self.xs_ids = [x.id for x in xs]
        self.pos = pos
        self.robot_poses = robot_poses

    def init_utility(self, U):
        """
        :param U: function taking  an int i as parameter,
          and returning a 2D utility array, with the utility associated with
          the ith robot NOT executing the task as the first element and the
          utility associated with the ith robot executing the task as the
          second element.
        """
        self.U = U

    def receive_q(self, q_msg):
        self.qs[q_msg.sender_id] = q_msg.q_values
        # print(f'Function {self.id} received a q msg: {self.qs}')

    def compute_r(self, var_node, nb_robots):
        r = [-1e6, -1e6]
        for xs_value in np.identity(nb_robots):
            sig = self.U(xs_value, self, var_node)  # Must return a 2D np array
            print(
                f'From funNode, values {xs_value}, var {var_node.id}, U {sig}')
            for var_id, q_values in remove_key(self.qs, var_node.id).items():
                if var_id in self.xs_ids:
                    print(
                        f'Adding {q_values}, from var {var_id}')
                    sig += np.flip(q_values)
            r = np.maximum(r, sig)

        print(
            f'rMessage from function {self.id} to var {var_node.id}: {r}.')
        return r

    def compute_rs(self, nb_robots):
        r_msgs = {}
        for x in self.xs:
            r_msgs[x.id] = self.compute_r(x, nb_robots)
        return r_msgs

    def combi_sr(self, xs):
        """Generates an iterable for all the combination of allocations
        feasible for the task. In particular, this method builds such
        combinations for Single Robot tasks, so each element is just a zero
        vector with only one component equal to 1.
        :param xs: list, the variables adjacent to the task.
        :return: length x length identity matrix.
        """
        return np.identity(len(xs))
