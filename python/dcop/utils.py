def remove_key(dic, elements):
    res = dic.copy()
    try:
        for e in elements:
            if e in res:
                del res[e]
    except TypeError:
        if elements in res:
            del res[elements]
    return res
