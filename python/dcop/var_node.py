from utils import remove_key


class varNode():

    def __init__(self, id, T):
        """Initialize the var node.
        :param id: int, unique id of the variable node (robot's id).
        :param T: list, the tasks adjacent to the variable
          (i.e. the tasks the robot can execute).
        """
        self.id = id
        self.rs = {}
        self.T = T

    def receive_r(self, r_msg):
        self.rs[r_msg.sender_id] = r_msg.r_values
        # print(f'Variable {self.id} received an r msg: {self.rs}')

    def compute_q(self, fun_node):
        q = [-1e6, 0]
        for f_id, fr_values in remove_key(self.rs, fun_node).items():
            sig = fr_values[1]
            for gr_values in remove_key(self.rs, [fun_node, f_id]).values():
                # print(
                #     f'From compute_q in var {self.id} for fun_node '
                #     f'{fun_node}, gr_value is {gr_values}')
                sig += gr_values[0]
            q[0] = max(q[0], sig)
            q[1] += fr_values[0]
        a = -(sum(q))/2
        return [a+q[0], a+q[1]]

    def compute_qs(self):
        q_msgs = {}
        for f in self.T:
            q_msgs[f] = self.compute_q(f)
        return q_msgs

    def make_decision(self):
        t_max = []
        u_max = -1e6
        for t in self.T:
            z = self.rs[t][1]
            for r_values in remove_key(self.rs, t).values():
                z += r_values[0]
            print(f'Variable {self.id} considered function {t}'
                  f' with utility {z}.')
            if z > u_max:
                t_max = t
                u_max = z
        return t_max
