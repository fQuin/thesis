"""Defines the constraints for the DCOP MRP inspired of the MDVRP presented in
Léauté, Thomas & Ottens, Brammert & Faltings, Boi. (2010). Ensuring Privacy
through Distributed Computation in Multiple-Depot Vehicle Routing Problems.
"""
import numpy as np


def tsp_cost_csts(nb_r, vars, w_poses):
    """Defines the intentional csts to minimize tour length
    :param nb_r: int, number of robots
    :param vars: dic, descriptions for pyDCOP vars
    :param w_poses: iterable, the poses of the waypoints
    """
    tl_csts = {}
    # Intentional constraints for tour length
    for r in range(nb_r):
        tl_csts[f"cost_{r}"] = {
            "type": "intention",
            "function": (
                "if 'solve_tsp_local_search' not in dir():\n"
                "   from python_tsp.heuristics import"
                " solve_tsp_local_search\n"
                "if 'numpy' not in dir(): import numpy\n"
                "v = []\n"
                "w_poses = ["+",".join([str(list(p)) for p in w_poses])+"]\n"
                "vars = [" + ",".join([f"{v}" for v in vars.keys()
                                       if int(v.split('_')[-2]) == r]) + "]\n"
                "for w, wp in enumerate(w_poses):\n"
                "   if vars[w]:\n"
                "       v.append(wp)\n"
                "if len(v) <= 1: return 0\n"
                "r_v = numpy.repeat(v, len(v), axis=0).reshape("
                "len(v), len(v), 2)\n"
                "distance_matrix = numpy.linalg.norm(r_v - numpy.transpose("
                "r_v, axes=(1, 0, 2)), axis=2)\n"
                "if len(v) == 2:\n"
                "   return distance_matrix[0][1] + distance_matrix[1][0]\n"
                "# print(distance_matrix)\n"
                "permutation, distance = "
                "solve_tsp_local_search(distance_matrix)\n"
                "return distance"
                )
            }
    return tl_csts


def tsp_cost_csts_implicit(nb_r, vars, w_poses):
    """Defines the intentional csts to minimize tour length with implicit model
    :param nb_r: int, number of robots
    :param vars: dic, descriptions for pyDCOP vars
    :param w_poses: iterable, the poses of the waypoints
    """
    tl_csts = {}
    for r in range(nb_r):
        tl_csts[f"cost_{r}"] = {
            "type": "intention",
            "function": (
                "if 'solve_tsp_local_search' not in dir():\n"
                "   from python_tsp.heuristics import"
                " solve_tsp_local_search\n"
                "if 'numpy' not in dir(): import numpy\n"
                "v = []\n"
                "w_poses = ["+",".join([str(list(p)) for p in w_poses])+"]\n"
                "vars = [" + ",".join([f"{v}" for v in vars.keys()]) + "]\n"
                "for w, wp in enumerate(w_poses):\n"
                f"   if vars[w] == {r}:\n"
                "       v.append(wp)\n"
                "if len(v) <= 1: return 0\n"
                "r_v = numpy.repeat(v, len(v), axis=0).reshape("
                "len(v), len(v), 2)\n"
                "distance_matrix = numpy.linalg.norm(r_v - numpy.transpose("
                "r_v, axes=(1, 0, 2)), axis=2)\n"
                "if len(v) == 2:\n"
                "   return distance_matrix[0][1] + distance_matrix[1][0]\n"
                "# print(distance_matrix)\n"
                "permutation, distance = "
                "solve_tsp_local_search(distance_matrix)\n"
                "return distance**4"
                )
            }
    return tl_csts


def tsp_max_csts_implicit(nb_r, vars, w_poses):
    """Defines the intentional csts to minimize tour length with implicit model
    :param nb_r: int, number of robots
    :param vars: dic, descriptions for pyDCOP vars
    :param w_poses: iterable, the poses of the waypoints
    """
    tl_csts = {}
    for r in range(nb_r):
        tl_csts["cost"] = {
            "type": "intention",
            "function": (
                "if 'solve_tsp_local_search' not in dir():\n"
                "   from python_tsp.heuristics import"
                " solve_tsp_local_search\n"
                "if 'numpy' not in dir(): import numpy\n"
                "v = []\n"
                "w_poses = ["+",".join([str(list(p)) for p in w_poses])+"]\n"
                "vars = [" + ",".join([f"{v}" for v in vars.keys()]) + "]\n"
                "m = -1\n"
                f"for r in range({nb_r}):\n"
                "   for w, wp in enumerate(w_poses):\n"
                "      if vars[w] == r:\n"
                "           v.append(wp)\n"
                "   if len(v) <= 1:\n"
                "       m = max([0, m])\n"
                "       continue\n"
                "   r_v = numpy.repeat(v, len(v), axis=0).reshape("
                "len(v), len(v), 2)\n"
                "   distance_matrix = numpy.linalg.norm(r_v - numpy.transpose("
                "r_v, axes=(1, 0, 2)), axis=2)\n"
                "   if len(v) == 2:\n"
                "       m = max([m, distance_matrix[0][1] + "
                "distance_matrix[1][0]])\n"
                "       continue\n"
                "   permutation, distance = "
                "   solve_tsp_local_search(distance_matrix)\n"
                "   m = max([m, distance])\n"
                "return m"
                )
            }
    return tl_csts


def visit_csts(nb_w, vars):
    """Defines the pseudo_hard csts to ensure that each waypoint is visited
    :param nb_w: int, number of waypoints
    :param vars: dic, descriptions for pyDCOP vars
    """
    vt_csts = {}
    # Each waypoint must be visited at least once
    for w in range(nb_w):
        vt_csts[f"visited_{w}"] = {
            "type": "intention",
            "function": ("0 if sum(["
                         + ",".join([f"{v}" for v in vars.keys()
                                     if int(v.split('_')[-1]) == w])
                         + "]) == 1 else 1000")
            }
    return vt_csts


def visit_csts_extensional(nb_w, nb_r, vars):
    """Defines the pseudo_hard csts to ensure that each waypoint is visited
    :param nb_w: int, number of waypoints
    :param vars: dic, descriptions for pyDCOP vars
    """
    vt_csts = {}
    Id = np.identity(nb_r, np.uint8)
    values = " | ".join([str(i) for i in Id]).replace("[", "").replace("]", "")
    # Each waypoint must be visited at least once
    for w in range(nb_w):
        vt_csts[f"visited_{w}"] = {
            "type": "extensional",
            "variables": [v for v in vars.keys()
                          if int(v.split('_')[-1]) == w],
            "values": {-1000: values}
            }
        """
        ("0 if sum(["
                     + ",".join([f"{v}" for v in vars.keys()
                                 if int(v.split('_')[-1]) == w])
                     + "]) == 1 else 1000")
        """
    return vt_csts
