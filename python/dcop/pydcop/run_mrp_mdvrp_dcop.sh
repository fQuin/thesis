python mrp_mdvrp_to_yaml.py

now=$(date +"%m_%d_%Y_%T")

pydcop --output metrics/output_${now}.json \
solve -a mgm2 --algo_params stop_cycle:20 \
-d test_mrpp_mdvrp_distribution.yaml -m thread \
--collect_on cycle_change --run_metrics metrics/run_time_${now}.csv \
test_mrpp_mdvrp_inst.yaml

python res_summary.py \
--input_file=metrics/output_${now}.json \
--output_file=metrics/summary_${now}.csv
