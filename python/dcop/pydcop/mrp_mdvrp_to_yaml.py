"""This module builds upon the constraints defined in mrp_mdvrp_constraints.py
in order to write down a pyDCOP instance YAML file.
"""

import yaml
import numpy as np
from mrp_mdvrp_constraints import (tsp_cost_csts,
                                   visit_csts,
                                   visit_csts_extensional,
                                   tsp_cost_csts_implicit)


def create_dcop(nb_r, nb_w, r_sen, w_req, w_poses,
                algorithm="mgm2",
                visit_csts_type="implicit",
                instance_file_name="./test_mrpp_mdvrp_inst.yaml",
                distribution_file_name="./test_mrpp_mdvrp_distribution.yaml"):
    domains = _create_domains(nb_w, nb_r, w_req, r_sen,
                              visit_csts_type=visit_csts_type)
    vars = _create_vars(nb_w, nb_r, visit_csts_type)
    csts = _create_constraints_flat(
       nb_w, nb_r, vars, w_poses[:nb_w], visit_csts_type)
    agents = _create_agents(nb_r)

    RES_DIC = {"name": "test_mrpp",
               "objective": "min",
               "domains": domains,
               "variables": vars,
               "constraints": csts,
               "agents": agents}

    with open(instance_file_name, 'w') as file:
        yaml.dump(RES_DIC, file, default_flow_style=False)

    distrib = _create_distrib(
        agents, vars, visit_csts_type, algorithm=algorithm)

    distrib_dic = {"distribution": distrib}

    with open(distribution_file_name, 'w') as file:
        yaml.dump(distrib_dic, file, default_flow_style=False)


def _create_vars(nb_w, nb_r, visit_csts_type):
    """Boolean var x_r^w is 1 if robot i survey waypoint w, 0 otherwise.
    Or, if visit_csts_type == "implicit", var x^w indicated the id of the robot
    surveying w.
    :param nb_w: int, number of waypoints of the mission
    :param nb_r: int, number of robots of the team
    :visit_csts_type: str from {"intentional", "extensional", "implicit"}, the
    type of constraints expressing that waypoints must be visited exactly once.
    """
    vars = {}
    if visit_csts_type == "implicit":
        for w in range(nb_w):
            vars[f"x_{w}"] = {"domain": f'd_{w}', "initial_value": w % nb_r}
    else:
        for w in range(nb_w):
            for r in range(nb_r):
                vars[f"x_{r}_{w}"] = {"domain": 'd1', "initial_value": 0}
    return vars


def _create_constraints_flat(nb_w, nb_r, vars, w_poses, visit_csts_type):
    """Builds the constraints.
    :param nb_w: int, number of waypoints of the mission
    :param nb_r: int, number of robots of the team
    :param vars: dic built using create_vars, describes the DCOP variables
    :param w_poses: 2D array, describes the waypoints' positions
    :visit_csts_type: str from {"intentional", "extensional", "implicit"}, the
    type of constraints expressing that waypoints must be visited exactly once.
    """
    if visit_csts_type == "intentional":
        csts = visit_csts(nb_w, vars)
    elif visit_csts_type == "extensional":
        csts = visit_csts_extensional(nb_w, nb_r, vars)

    if visit_csts_type == "implicit":
        csts = tsp_cost_csts_implicit(nb_r, vars, w_poses)
    else:
        csts.update(tsp_cost_csts(nb_r, vars, w_poses))
    return csts


def _create_agents(nb_r):
    """Defines the agents.
    :param nb_r: int, number of robots of the team
    """
    agents = {}
    for r in range(nb_r):
        agents[f'r_{r}'] = {'capacity': 1000}
    return agents


def _create_distrib(agents, vars, visit_csts_type, algorithm="mgm2"):
    """Builds the distribution yaml file to indicate which agents does what.
    :param agents: dic, generated with _create_agents()
    :param vars: dic, generated with _create_vars()
    :visit_csts_type: str from {"intentional", "extensional", "implicit"}, the
    type of constraints expressing that waypoints must be visited exactly once.
    :param algorithm: str, the pyDCOP algorithm that will be used.
    """
    res = {}
    for a in agents:
        if visit_csts_type == "implicit":
            res[a] = [v for v in vars
                      if (int(v.split('_')[-1]) + 1) % len(
                          agents) == int(a.split('_')[-1])]
        else:
            res[a] = [v for v in vars if v.split('_')[-2] == a.split('_')[-1]]
    if algorithm in ["maxsum"]:
        # Need to specify the distribution of the constraints nodes
        for a in agents:
            res[a].append(f"cost_{int(a.split('_')[-1])}")
    return res


def _create_domains(nb_w, nb_r, w_req, r_sen, visit_csts_type="implicit"):
    """Defines the variables domains.
    :param nb_w: int, number of waypoints of the mission
    :param nb_r: int, number of robots of the team
    :param w_req: dic, keys are waypoint ids, values are sensor requirements
    :param r_sen: dic, keys are robots ids, values are avalable sensors
    :visit_csts_type: str from {"intentional", "extensional", "implicit"}, the
    type of constraints expressing that waypoints must be visited exactly once.
    """
    if visit_csts_type == "implicit":
        res = {f'd_{w}': {"values": [r for r in range(nb_r)
                                     if set(w_req[w]) & set(r_sen[r])]
                          }
               for w in range(nb_w)}
    else:
        # TODO: Sensor match for non implicit models
        res = {"d1": {"values": [0, 1]}}
    return res


if __name__ == "__main__":
    # "extensional", "intentional" or "implicit"
    ALG = "mgm2"
    VISIT_CST_TYPE = "implicit"
    R = 5
    W = 20
    ROBOT_SENSORS = {
        r: ["camera", "nightcam"] if not r % 2 else ["camera"]
        for r in range(R)
    }
    WAYPOINTS_REQUIREMENTS = {
        w: ["nightcam"] if not w % 5 else ["camera", "nightcam"]
        for w in range(W)
    }
    WP = np.array([[0, 0],
                   [0, 1],
                   [1, 1],
                   [5, 5],
                   [5, 7],
                   [7, 5],
                   [6, 6],
                   [15, 1],
                   [15, 0],
                   [16, 1],
                   [3, 3],
                   [3, 1],
                   [4, 1],
                   [4, 5],
                   [4, 7],
                   [7, 10],
                   [6, 10],
                   [15, 10],
                   [15, 9],
                   [16, 10]])

    create_dcop(R, W, ROBOT_SENSORS, WAYPOINTS_REQUIREMENTS, WP, algorithm=ALG)
