"""Defines the constraints for the DCOP MRP.
"""
import numpy as np


def tour_length_csts(nb_r, vars, w_poses):
    """Defines the intentional csts to minimize tour length
    :param nb_r: int, number of robots
    :param vars: dic, descriptions for pyDCOP vars
    :param w_poses: iterable, the poses of the waypoints
    """
    tl_csts = {}
    # Intentional constraints for tour length
    for r in range(nb_r):
        tl_csts[f"cost_{r}"] = {
            "type": "intention",
            "function": "sum(["
            + ",".join([f"{v}*{np.linalg.norm(w_poses[int(v.split('_')[-3])] - w_poses[int(v.split('_')[-2])])}"
                        for v in vars.keys()
                        if int(v.split('_')[-1]) == r]) + "])"}
    return tl_csts


def visit_csts(nb_w, vars):
    """Defines the pseudo_hard csts to ensure that each waypoint is visited
    :param nb_w: int, number of waypoints
    :param vars: dic, descriptions for pyDCOP vars
    """
    vt_csts = {}
    # Each waypoint must be visited at least once
    for w in range(nb_w):
        vt_csts[f"visited_{w}"] = {
            "type": "intention",
            "function": ("0 if sum(["
                         + ",".join([f"{v}" for v in vars.keys()
                                     if int(v.split('_')[-2]) == w])
                         + "]) > 0 else 1000")
            }
    return vt_csts


def flow_csts(nb_r, nb_w, vars):
    """Defines the pseudo_hard csts to ensure flow conservation
    :param nb_r: int, number of robots
    :param nb_w: int, number of waypoints
    :param vars: dic, descriptions for pyDCOP vars
    """
    fc_csts = {}
    for w in range(nb_w):
        for r in range(nb_r):
            fc_csts[f"flow_{w}_{r}"] = {
                "type": "intention",
                "function": ("0 if sum(["
                             + ",".join([f"{v}" for v in vars.keys()
                                         if (int(v.split('_')[-2]) == w
                                         and (int(v.split('_')[-1]) == r))])
                             + "]) == sum(["
                             + ",".join([f"{v}" for v in vars.keys()
                                         if (int(v.split('_')[-3]) == w
                                         and (int(v.split('_')[-1]) == r))])
                             + "]) else 100")
            }
    return fc_csts


def subtour_csts(nb_r, nb_w, vars):
    """Defines the pseudo_hard csts to remove subtours. Doing so is quite easy
    since the xs values are known. The constraints check if a tour beginning
    on some waypoint visits all waypoint that are visited.
    :param nb_r: int, number of robots
    :param nb_w: int, number of waypoints
    :param vars: dic, descriptions for pyDCOP vars
    """
    se_csts = {}
    for r in range(nb_r):
        se_csts[f"tour_{r}"] = {
            "type": "intention",
            "function": (
                "visited = []\n"
                "var_list = ["
                + ",".join([f"{v}" for v in vars.keys()
                            if int(v.split('_')[-1]) == r])
                + "]\n"
                "path_dic = {}\n"
                "value_dic = " + str({
                    w: [v for v in vars.keys()
                        if (int(v.split('_')[-1]) == r
                        and int(v.split('_')[-3]) == w)]
                    for w in range(nb_w)}).replace("'", "") + "\n"
                "# print('Print de value_dic', value_dic)\n"
                "target_dic = " + str({
                    w: [int(v.split('_')[-2]) for v in vars.keys()
                        if (int(v.split('_')[-1]) == r
                        and int(v.split('_')[-3]) == w)]
                    for w in range(nb_w)}).replace("'", "") + "\n"
                "# print('Print de target_dic', target_dic)\n"
                f"for w in range({nb_w}):\n"
                "    w_vars = [v for v in value_dic[w]]\n"
                "    # print('    ', w, w_vars, sum(w_vars))\n"
                "    if sum(w_vars) > 0:\n"
                "        arg_max = max(zip(w_vars, range(len(w_vars))))[1]\n"
                "        path_dic[w] = target_dic[w][arg_max]\n"
                "# print(path_dic)\n"
                "visited = []\n"
                "if path_dic:\n"
                "    # print('Print de path_dic: ', path_dic)\n"
                "    tmp_w = list(path_dic.keys())[0]\n"
                "    for s, t in path_dic.items():\n"
                "        if tmp_w not in visited:\n"
                "            visited.append(tmp_w)\n"
                "        try:\n"
                "           tmp_w = path_dic[tmp_w]\n"
                "        except KeyError:\n"
                "           return 100\n"
                f"# print({r}, visited, sum(var_list), var_list, path_dic)\n"
                "return 0 if len(visited) == len(path_dic) else 100"
                )
        }
    return se_csts
