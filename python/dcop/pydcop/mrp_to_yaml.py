import yaml
import numpy as np
from mrp_constraints import (tour_length_csts,
                             visit_csts,
                             flow_csts,
                             subtour_csts)


def create_vars(nb_w, nb_r, sensors_list):
    vars = {}
    for i in range(nb_w):
        for j in range(nb_w):
            if not i == j:
                for r in range(nb_r):
                    vars[f"x_{i}_{j}_{r}"] = {"domain": 'd1',
                                              "initial_value": 1,
                                              "sensors": sensors_list[r]}

    return vars


def create_constraints(nb_w, nb_r, vars):
    csts = {}
    # Intentional constraints for tour length
    for r in range(nb_r):
        csts[f"cost_{r}"] = {
            "type": "intention",
            "source": "./mrp_constraints.py",
            "function": "source.tour_length(["
            + ",".join([f"{v}" for v in vars.keys()
                        if int(v.split('_')[-1]) == r]) + "])"}

    for w in range(nb_w):
        csts[f"visited_{w}"] = {
            "type": "intention",
            "source": "./mrp_constraints.py",
            "function": "source.is_visited(["
            + ",".join([f"{v}" for v in vars.keys()
                        if int(v.split('_')[-2]) == w]) + "])"}
    return csts


def create_constraints_flat(nb_w, nb_r, vars, w_poses):
    # Intentional constraints for tour length
    csts = tour_length_csts(nb_r, vars, w_poses)

    # Each waypoint must be visited at least once
    csts.update(visit_csts(nb_w, vars))

    # Flow conservation constraints
    csts.update(flow_csts(nb_r, nb_w, vars))

    # There must be exactly one tour for each robot
    csts.update(subtour_csts(nb_r, nb_w, vars))
    return csts


def create_agents(vars):
    agents = {}
    for vid, var in vars.items():
        agents[vid] = {}
    return agents


def create_ext_vars(w_poses, r_poses):
    ext_vars = {}
    for indi, i in enumerate(w_poses):
        for indj, j in enumerate(w_poses):
            if not indi == indj:
                d_ij = np.linalg.norm(i-j)
                ext_vars[f"dist_{i}_{j}"] = {"domain": [d_ij],
                                             "initial_value": d_ij}


if __name__ == "__main__":
    ALGO = "dpop"
    R = 2
    W = 4
    S = ["camera"]*R
    T = ["UGV"]*R
    WP = np.array([[0, 0],
                   [0, 1],
                   [1, 1],
                   [1, 0],
                   [2, 1],
                   [2, 0]])
    RP = np.array([[1, 0],
                   [0, -1]])

    DOMAINS = {"d1": {"values": [0, 1]}}
    VARS = create_vars(W, R, S)
    EXT_VARS = create_ext_vars(WP, RP)
    CSTS = create_constraints_flat(W, R, VARS, WP)
    AGENTS = create_agents(VARS)

    RES_DIC = {"name": "test_mrpp",
               "objective": "min",
               "domains": DOMAINS,
               "variables": VARS,
               "constraints": CSTS,
               "agents": AGENTS}

    with open("./test_mrpp_inst.yaml", 'w') as file:
        yaml.dump(RES_DIC, file, default_flow_style=False)
