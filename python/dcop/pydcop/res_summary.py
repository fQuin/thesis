"""Reads the results from an pyDCOP output file to generate a summary taylored
to MRPP needs.
"""

import json
import argparse
import numpy as np
from tabulate import tabulate
from python_tsp.exact import solve_tsp_dynamic_programming


if __name__ == "__main__":
    WP = np.array([[0, 0],
                   [0, 1],
                   [1, 1],
                   [5, 5],
                   [5, 7],
                   [7, 5],
                   [6, 6],
                   [15, 1],
                   [15, 0],
                   [16, 1],
                   [3, 3],
                   [3, 1],
                   [4, 1],
                   [4, 5],
                   [4, 7],
                   [7, 10],
                   [6, 10],
                   [15, 10],
                   [15, 9],
                   [16, 10]])
    parser = argparse.ArgumentParser()

    parser.add_argument('--input_file', help='Input file, should be an output'
                        'from pyDCOP.')
    parser.add_argument('--output_file', help='Ouput CSV file.')

    args = parser.parse_args()

    with open(args.input_file) as input_file:
        input_data = json.load(input_file)

    headers = ["Status", "Time", "Cycles", "Max Length", "Mean Length",
               "Total Length"]

    res = {"Status": "Unknown",  # TODO: how to determine if solution feasible
           "Time": input_data['time'],
           "Cycles": input_data['cycle'],
           "Max Length": -1, "Mean Length": -1, "Total Length": -1}

    tour_lengths = {a: -1 for a in input_data["agt_metrics"].keys()}

    for a in tour_lengths.keys():
        v = []
        a_id = a.split('_')[-1]
        for w, wp in enumerate(WP):
            if input_data['assignment']['x_'+str(w)] == int(a_id):
                v.append(wp)
        if len(v) <= 1:
            tour_lengths[a] = 0
            continue
        r_v = np.repeat(v, len(v), axis=0).reshape(len(v), len(v), 2)
        distance_matrix = np.linalg.norm(r_v - np.transpose(
            r_v, axes=(1, 0, 2)), axis=2)
        if len(v) == 2:
            tour_lengths[a] = distance_matrix[0][1] + distance_matrix[1][0]
            continue
        permutation, tour_lengths[a] = solve_tsp_dynamic_programming(
            distance_matrix)

    res["Max Length"] = np.max(list(tour_lengths.values()))
    res["Mean Length"] = np.mean(list(tour_lengths.values()))
    res["Total Length"] = np.sum(list(tour_lengths.values()))

    with open(args.output_file, 'w+') as output_file:
        json.dump(res, output_file)

    print(tabulate([list(res.values())], headers=list(res.keys())))
