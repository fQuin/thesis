from fun_node import funNode
from var_node import varNode
from msgs import rMessage, qMessage
import numpy as np
import time


def rand_capabilities(r, n, size=3):
    res = np.empty((r, size))
    for i in range(r):
        res[i] = np.random.choice(np.arange(n), size=size, replace=False)
    for t in range(n):
        if t not in np.array(res).flatten():
            res = rand_capabilities(r, n, size)
    return np.int16(res)


def patrol_utility():
    pass


def dist_utility(vars, fun, var_node):
    res = [0, 0]
    true_j_vars = list(vars)
    true_j_vars[var_node.id] = 1
    if not sum(true_j_vars) == 1:
        res[1] = 10  # Penalty if the constraint is not fulfiled
    else:
        res[1] = np.linalg.norm(fun.pos-fun.robot_poses[var_node.id])
    false_j_vars = list(vars)
    false_j_vars[var_node.id] = 0
    if not sum(false_j_vars) == 1:
        res[0] = 10  # Penalty if the constraint is not fulfiled
    else:
        res[0] = np.linalg.norm(fun.pos-fun.robot_poses[np.argmax(vars)])
    return -np.array(res)  # Distance must have negative utility


def utility():
    return dist_utility


def random_utility(caps, fun, rand_valuation_true, rand_valuation_false):
    return lambda ind: (
        np.array((rand_valuation_true[ind][fun],
                  rand_valuation_false[ind][fun]))
        if fun in caps[ind] else None)


if __name__ == "__main__":
    R = 5  # Number of robots
    N = 20  # Number of tasks
    # CAPABILITIES = rand_capabilities(R, N, R)

    # All robots have full capabilities
    CAPABILITIES = np.tile(np.arange(N), (R, 1))
    A = range(1, R+1)  # Agents, not really needed here
    # Variables
    X = [varNode(i, CAPABILITIES[i]) for i in range(R)]
    D = {x: x.T for x in X}  # Domains for assignment variables
    C = []  # Set of soft constraints ?
    # µ is already abstractly defined as each agent owns its variable.
    # RV_TRUE = np.random.uniform(size=(R, N))
    # RV_FALSE = np.random.uniform(size=(R, N))
    # UTILITY = random_utility
    ROBOT_POSES = np.random.uniform(size=(R, 2))
    # Functions
    F = [funNode(j,
                 [x for x in X if j in x.T],
                 np.random.uniform(size=2),
                 ROBOT_POSES)
         for j in range(N)]
    for f in F:
        f.init_utility(utility())

    print(CAPABILITIES)
    print([[np.linalg.norm(a-b.pos) for a in ROBOT_POSES] for b in F])

    EPSILON = 1e-4
    DELTA = 1
    START_TIME = time.time()
    RUN_TIME = 0
    TIME_LIMIT = 10
    OCC = 0
    MAX_OCC = 20
    while EPSILON < DELTA and RUN_TIME < TIME_LIMIT and OCC < MAX_OCC:
        # Computing the messages from functions to variables
        r_msgs = {}
        for f in F:
            r_msgs[f.id] = f.compute_rs(R)
            # print(f'f_id {f.id}, msg {r_msgs[f.id]}')
            # print(f'Lambda(0): {random_utility(
            #     CAPABILITIES, f.id, RV_TRUE, RV_FALSE)(0)}'
            #       f'Lambda(1): {random_utility(
            #     CAPABILITIES, f.id, RV_TRUE, RV_FALSE)(1)}')
        print(f'Occurence {OCC} r_msgs: {r_msgs}')
        # Sending the messages to the variables
        for f in F:
            for x in f.xs:
                x.receive_r(rMessage(f.id, r_msgs[f.id][x.id]))
        # Computing the messages from variables to functions
        q_msgs = {}
        for x in X:
            q_msgs[x.id] = x.compute_qs()
        # print(f'Occurence {OCC} q_msgs: {q_msgs}')
        # Sending the messages to the variables
        for x in X:
            for f in F:
                if f.id in x.T:
                    f.receive_q(qMessage(x.id, q_msgs[x.id][f.id]))
        # Making a temporary decision for all variables
        tmp_alloc = {}
        for x in X:
            tmp_alloc[x.id] = x.make_decision()
        print(f'qMessages: {q_msgs}')
        print('Current allocation: ')
        print("".join(f'{x.id}: {tmp_alloc[x.id]}\n' for x in X))
        RUN_TIME = time.time() - START_TIME
        OCC += 1
