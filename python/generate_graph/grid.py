# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 21:49:23 2022

@author: fequi
"""

import networkx as nx
import numpy as np

from intersect import Point, doIntersect


class my_grid_graph(nx.Graph):
    
    def __init__(self, grid_dimensions, obstacles_prob,
                 periodic=False, obstacle_margin=1):
        """ Creates an grid graph with dimensions grid_dimensions.
    
        The dimension is the length of the list 'grid_dimensions' and the
        size in each dimension is the value of the list element.
    
        E.g. G=my_grid_graph(grid_dimensions=[2,3]) produces a 2x3 grid graph.
        
        obstacles_prob indicates the probability of a node to be the upper
        left corner of a square obstacle.
    
        If periodic=True then join grid edges with periodic boundary
        conditions.
        
        obstacle_margin is the space between obstacles and graph edges.
    
        """        
        super().__init__(name="grid_graph")
        
        self.obstacle_margin = obstacle_margin

        prod = 1
        for d in grid_dimensions:
            prod *= d
        new_labels = np.arange(prod)   

        G = self._init_linear_graph(grid_dimensions, periodic)
        self.graph = nx.relabel_nodes(
                G, {n: new_labels[ind] for ind, n in enumerate(G.nodes)})

        self.dim = grid_dimensions
        
        self.remove_edges((len(self.graph.edges)-len(self.graph.nodes))/2)
    
        self.obstacle_corners = self.select_squares(obstacles_prob)
        self.com_prob_dic = self.make_edge_pair_dic()
        
        
    def _init_linear_graph(self, dim, periodic):
        """ Creates a linear graph used to initiliaze the grid graph.
        :param dim: list of integers, the dimensions of the grid graph.
        :param periodic: boolean
        :return G: nx.Graph instance.
        """
        
        if periodic:
            func = nx.cycle_graph
        else:
            func = nx.path_graph
    
        dim = list(dim)
        current_dim = dim.pop()
        G = func(current_dim)
        while len(dim) > 0:
            current_dim = dim.pop()
            # order matters: copy before it is cleared during the creation of Gnew
            Gold = G.copy()
            Gnew = func(current_dim)
            # explicit: create_using=None
            # This is so that we get a new graph of Gnew's class.
            G = nx.cartesian_product(Gnew,Gold)
        # graph G is done but has labels of the form (1,(2,(3,1)))
        
        for n in G.nodes:
            G.nodes[n]["pos"] = [n[0]*100, n[1]*100]
            print([n[0]*100, n[1]*100])
        
        return G

    def remove_edges(self, n, max_it=1e4):
        """Removes random edges from the graph
        :param n: int, the maximum number of edges to remove.
        :param max_it: int, the maximum number of tries.
        """
        nb_removed = 0
        nb_it = 0
        while nb_removed < n and nb_it < max_it:
            H = self.graph.copy()
            h_edge_list = [e for e in self.graph.edges]
            tmp_edge = h_edge_list[
                    int(np.random.choice(np.arange(len(self.graph.edges))))]
            H.remove_edge(*tmp_edge)
            if nx.is_connected(self.graph):
                self.graph = H.copy()
                nb_removed += 1
            nb_it += 1

    def select_squares(self, p):
        square_poses = []
        maxnb_squares = (self.dim[0] - 1) * (self.dim[1] - 1)
        for x in range(self.dim[0]-1):
            for y in range(self.dim[1]-1):
                if np.random.rand() < p:
                    square_poses.append((x, y))
        # Random selection must account for the distribution of the
        # obstacles. If the number of obstacles is an outlier, we
        # must discard them.
        if (len(square_poses) > maxnb_squares*(0.5+1/12**.5)
            or len(square_poses) < maxnb_squares*(0.5-1/12**.5)):
            return self.select_squares(p)
        return square_poses
    
    def make_edge_pair_dic(self):
        res = {}
        for e1 in self.graph.edges:
            e1_source_pos = self.graph.nodes[e1[0]]["pos"]
            e1_target_pos = self.graph.nodes[e1[1]]["pos"]
            e1_pos = Point(
                    (e1_source_pos[0]+e1_target_pos[0])/2,
                    (e1_source_pos[1]+e1_target_pos[1])/2)
            for e2 in self.graph.edges:
                if not (e1[0] == e2[0] and e1[1] == e2[1]):
                    e2_source_pos = self.graph.nodes[e2[0]]["pos"]
                    e2_target_pos = self.graph.nodes[e2[1]]["pos"]
                    e2_pos = Point(
                            (e2_source_pos[0]+e2_target_pos[0])/2,
                            (e2_source_pos[1]+e2_target_pos[1])/2)
                    res[str((e1, e2))] = 0
                    for obs in self.obstacle_corners:
                        obs_x = obs[0]*100
                        obs_y = obs[1]*100
                        # First segment
                        s1_pos = Point(
                                obs_x + self.obstacle_margin,
                                obs_y + self.obstacle_margin)
                        s2_pos = Point(
                                obs_x + 100 - self.obstacle_margin,
                                obs_y + self.obstacle_margin)
                        s3_pos = Point(
                                obs_x + self.obstacle_margin,
                                obs_y + 100 - self.obstacle_margin)
                        s4_pos = Point(
                                obs_x + 100 - self.obstacle_margin,
                                obs_y + 100 - self.obstacle_margin)
                        if (doIntersect(e1_pos, e2_pos, s1_pos, s2_pos)
                                or doIntersect(e1_pos, e2_pos, s2_pos, s4_pos)
                                or doIntersect(e1_pos, e2_pos, s1_pos, s3_pos)
                                or doIntersect(e1_pos, e2_pos, s3_pos, s4_pos)):
                            res[str((e1, e2))] = 1
                            
        return res
                    
                

G = my_grid_graph((5, 4), 0.5)

print(G.obstacle_corners)

print(G.com_prob_dic)

