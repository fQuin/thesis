import numpy as np
import time
from task import Task
from agent import Agent


if __name__ == "__main__":
    R = 2  # Number of robots
    N = 3  # Number of tasks
    # CAPABILITIES = rand_capabilities(R, N, R)

    TASKS = [Task(i, np.random.uniform(size=2)) for i in range(N)]

    AGENTS = [Agent(i, TASKS, np.random.uniform(size=2), R) for i in range(R)]

    for t in TASKS:
        print(f'Task {t.id} is at coordinates {t.pos}.')

    for a in AGENTS:
        print(f'Agent {a.id} is at coordinates {a.pos}.')

    EPSILON = 1e-4
    DELTA = 1e3
    START_TIME = time.time()
    RUN_TIME = 0
    TIME_LIMIT = 10
    OCC = 0
    MAX_OCC = 2
    while EPSILON < DELTA and RUN_TIME < TIME_LIMIT and OCC < MAX_OCC:
        for a in AGENTS:
            a.build_bundle()

        for a in AGENTS:
            print(f'After the auction phase, agent {a.id}'
                  f' has path {[t.id for t in a.path]}')

        for a in AGENTS:
            tmp_cbba_msg = a.build_cbba_msg()
            print(f'Agent {a.id} has msg {vars(tmp_cbba_msg)}.')
            other_as = AGENTS[:]
            other_as.remove(a)
            for other_a in other_as:
                other_a.receive_vectors(tmp_cbba_msg)

        # TODO: DELTA = ?
        RUN_TIME = time.time() - START_TIME
        OCC += 1
