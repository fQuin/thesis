import numpy as np
import time
from cbbaMsg import CbbaMsg


class Agent():

    def __init__(self, id, tasks, pos, nb_agents):
        """Initialize the agent class.
        :param id: int, unique agent id.
        :param tasks: type Task, an ordered vector of task objects.
        :param pos: 2d array, the starting position of the agent.
        :param nb_agents: int, the number of agents in the robotic team.
        """
        self.id = id
        self.pos = pos
        self.N = len(tasks)
        self.xs = np.zeros(self.N)
        self.ys = np.zeros(self.N)
        self.zs = np.zeros(self.N)-1
        self.tasks = tasks
        self.tasks_poses = np.array([t.pos for t in self.tasks])
        self.tasks_ids = np.array([t.id for t in self.tasks])
        self.plan = []
        self.bundle = []
        self.path = []
        self.ss = np.zeros(nb_agents)

    def select_task(self):
        """SelectTask procedure from Choi et al. 2009.
        """
        cs = np.linalg.norm(
            np.tile(self.pos, (self.N, 1)) - self.tasks_poses, axis=1)**-1
        if not self.xs.sum():
            hs = np.greater(cs, self.ys)
            if hs.sum():
                self.J = np.argmax(hs*cs)
                self.xs[self.J] = 1
                self.ys[self.J] = cs[self.J]

    def build_bundle(self, verbose=False):
        """BuildBunle procedure from Choi et al. 2009
        Some computations seem unneeded. The "-S" in cs computation is useless
        In consequence, the isin_bundle is also useless as S matrix is already
        0 if t is in bundle.
        cs computation could be simplified as cs = (S_mat)**-1).max(axis=0)
        """
        if verbose:
            print("\n===============")
            print("Before: ")
            print(f'Ys vector: {self.ys}')
            print(f'Zs vector: {self.zs}')
            print(f'Bundle: {self.bundle}')
            print(f'Path: {self.path}')

        while len(self.bundle) < len(self.tasks):
            S = self.path_length(self.path)
            S_mat = self.build_S_matrix()
            isin_bundle = np.isin(self.tasks_ids, self.bundle, invert=True)
            cs = ((S_mat - S)**-1*isin_bundle).max(axis=0)
            hs = np.greater(cs, self.ys)
            J = np.argmax(hs*cs)
            n_J = np.argmax(S_mat[:, J])
            self.bundle.append(self.tasks[J])
            self.path.insert(n_J, self.tasks[J])
            self.ys[J] = cs[J]
            self.zs[J] = self.id

            if verbose:
                print("\n===============")
                print(f'At iteration {len(self.bundle)-1}: ')
                print(f'S matrix: {self.build_S_matrix()}')
                print(f'Cs vector: {cs}')
                print(f'J: {J}')
                print(f'n_J: {n_J}')
                print(f'Ys vector: {self.ys}')
                print(f'Zs vector: {self.zs}')
                print(f'Bundle: {[t.id for t in self.bundle]}')
                print(f'Path: {[t.id for t in self.path]}')

    def build_S_matrix(self):
        mat = np.zeros((len(self.path)+1, len(self.tasks)))
        for tind, t in enumerate(self.tasks):
            if t not in self.bundle:
                for ind in range(len(self.path)+1):
                    tmp_path = self.path.copy()
                    tmp_path.insert(ind, t)
                    mat[ind, tind] = self.path_length(tmp_path)
        return mat

    def path_length(self, path):
        start_dist = np.linalg.norm(self.pos-path[0].pos) if path else 0
        return start_dist + sum([np.linalg.norm(t.pos - path[i+1].pos)
                                 for i, t in enumerate(path[:-1])])

    def receive_vectors(self, msg):
        """Action rules for communication between two agents from Choi 2009.
        Remark: the if-else structures might be simplified but it'll be left
        similar to the paper's presentation in Table 1 to improve readability.
        :param msg: a CbbaMsg.
        """
        mod_ts = []
        # Update the s vector as agent received an update from sender
        self.ss[msg.sender] = time.time()

        # Starting the update rules according to Table 1.
        for t in self.tasks:
            if msg.zs[t.id] == msg.sender:
                if self.zs[t.id] == self.id:
                    if msg.ys[t.id] > self.ys[t.id]:
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                elif self.zs[t.id] == msg.sender:
                    mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                elif self.zs[t.id] == -1:
                    mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                else:
                    if (msg.ss[self.zs[t.id]] > self.ss[self.zs[t.id]]
                            or msg.ys[t.id] > self.ys[t.id]):
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
            elif msg.zs[t.id] == self.id:
                if self.zs[t.id] == msg.sender:
                    mod_ts.append(self.reset_yz(t.id))
                elif self.zs[t.id] not in (self.id, -1):
                    if msg.ss[self.zs[t.id]] > self.ss[self.zs[t.id]]:
                        mod_ts.append(self.reset_yz(t.id))
            elif msg.zs[t.id] == -1:
                if self.zs[t.id] == msg.sender:
                    mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                elif self.zs[t.id] not in (self.id, -1):
                    if msg.ss[self.zs[t.id]] > self.ss[self.zs[t.id]]:
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
            else:
                if self.zs[t.id] == self.id:
                    if (msg.ss[msg.zs[t.id]] > self.ss[msg.zs[t.id]]
                            and msg.ys[t.id] > self.ys[t.id]):
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                elif self.zs[t.id] == msg.sender:
                    if msg.ss[msg.zs[t.id]] > self.ss[msg.zs[t.id]]:
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                    else:
                        mod_ts.append(self.reset_yz(t.id))
                elif self.zs[t.id] == msg.zs[t.id]:
                    if msg.ss[msg.zs[t.id]] > self.ss[msg.zs[t.id]]:
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                elif self.zs[t.id] == -1:
                    if msg.ss[msg.zs[t.id]] > self.ss[msg.zs[t.id]]:
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                else:
                    if (msg.ss[msg.zs[t.id]] > self.ss[msg.zs[t.id]]
                            and msg.ss[self.zs[t.id]] > self.ss[self.zs[t.id]]):
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                    elif (msg.ss[msg.zs[t.id]] > self.ss[msg.zs[t.id]]
                            and msg.ys[t.id] > self.ys[t.id]):
                        mod_ts.append(self.update_yz(msg.ys, msg.zs, t.id))
                    elif (msg.ss[self.zs[t.id]] > self.ss[self.zs[t.id]]
                            and self.ss[msg.zs[t.id]] > msg.ss[msg.zs[t.id]]):
                        mod_ts.append(self.reset_yz(t.id))

        # Removing subsequent tasks from bundle, path, ys and zs if any task
        # was updated or reset
        self._release_tasks(mod_ts)

    def _release_tasks(self, mod_ts):
        common_inds = np.where(np.in1d(self.bundle, mod_ts))[0]
        if common_inds.size > 0:
            first_changed_task = common_inds[0]
            tasks_to_remove = self.bundle[first_changed_task:]
            self.bundle = self.bundle[:first_changed_task]
            self.ys[tasks_to_remove] = 0
            self.zs[tasks_to_remove] = -1
            path_reset_inds = np.where(np.in1d(self.path, tasks_to_remove))
            self.path = list(np.delete(self.path, path_reset_inds))

    def update_yz(self, ys, zs, ind=None):
        if ind is None:
            self.ys = ys
            self.zs = zs
        else:
            self.ys[ind] = ys[ind]
            self.zs[ind] = zs[ind]
        return ind

    def reset_yz(self, ind=None):
        if ind is None:
            self.ys = np.zeros(self.N)
            self.zs = np.zeros(self.N) - 1
        else:
            self.ys[ind] = 0
            self.zs[ind] = -1
        return ind

    def update_task(self, y_msg):
        """UpdateTask procedure from Choi et al. 2009.
        """
        if self.ys[self.J] < y_msg.ys[self.J]:
            self.xs[self.J] = 0
        self.ys = np.maximum(self.ys, y_msg.ys)

    def build_cbba_msg(self):
        return CbbaMsg(self.id, self.ys, self.zs, self.ss)
