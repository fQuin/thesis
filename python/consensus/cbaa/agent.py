import numpy as np
from yMsg import yMsg


class Agent():

    def __init__(self, id, tasks, pos):
        """Initialize the agent class.
        :param id: int, unique agent id.
        :param tasks: type Task, an ordered vector of task objects.
        :param pos: 2d array, the starting position of the agent.
        """
        self.id = id
        self.pos = pos
        self.N = len(tasks)
        self.xs = np.zeros(self.N)
        self.ys = np.zeros(self.N)
        self.tasks = tasks
        self.tasks_poses = np.array([t.pos for t in self.tasks])
        self.plan = []
        self.J = None

    def select_task(self):
        """SelectTask procedure from Choi et al. 2009.
        """
        cs = np.linalg.norm(
            np.tile(self.pos, (self.N, 1)) - self.tasks_poses, axis=1)**-1
        if not self.xs.sum():
            hs = np.greater(cs, self.ys)
            if hs.sum():
                self.J = np.argmax(hs*cs)
                self.xs[self.J] = 1
                self.ys[self.J] = cs[self.J]

    def update_task(self, y_msg):
        """UpdateTask procedure from Choi et al. 2009.
        """
        if self.ys[self.J] < y_msg.ys[self.J]:
            self.xs[self.J] = 0
        self.ys = np.maximum(self.ys, y_msg.ys)

    def build_y_msg(self):
        return yMsg(self.id, self.ys)
