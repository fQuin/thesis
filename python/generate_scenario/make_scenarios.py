# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 22:27:10 2022

@author: fequi
"""

import argparse
import os
from utils.plot_instances import plot_instance
from graphs.grid import generate_scenarios as gen_grid
from graphs.star import generate_scenarios as gen_star
from graphs.caylus import generate_scenarios as gen_caylus

nb_scenarios = 50

parser = argparse.ArgumentParser()

parser.add_argument('--graph_type',
                    help='The type of navigation graph shape:\n'
                    '  - grid: produces a grid graph, with nb_row and nb_col'
                    'defined by the corresponding arguments.\n'
                    '  - star: produces a star graph with nb_branch and '
                    'star_depth defined by the corresponding arguments.\n'
                    '  - isthme: todo \n'
                    '  - caylus: makes a Caylus instance. The node, edges and obstacles stay the same. Only the dynamic events vary. \n')

parser.add_argument('--dir', help='Path to the target directory.')

parser.add_argument('--data_path', help='Path to the data JSON for Caylus graph.')

parser.add_argument('--nb_row', help='Number of rows of a grid graph.')
parser.add_argument('--nb_col', help='Number of columns of a grid graph.')

parser.add_argument('--star_depth', help='Depth of branches in a star graph.')
parser.add_argument('--nb_branch', help='Number of branches of a star graph.')

parser.add_argument('--obs_prob',
                    help='Probability for a node of the graph to be the'
                    'corner of an obstacle.')

parser.add_argument('--nb_scenarios',
                    help='Number of scenarios to be generated.')

args = parser.parse_args()

if os.path.isdir(args.dir):
    dir_name = args.dir
else:
    raise FileNotFoundError(f'{args.dir} is not a directory.')

try:
    nb_scenarios = int(args.nb_scenarios)
except ValueError:
    raise TypeError(f'Type of nb_scenarios is not a python int. '
                    'Setting nb_scenarios to default '
                    f'value, nb_scenarios={nb_scenarios}.')

if args.graph_type == "grid":
    try:
        nb_row = int(args.nb_row)
    except ValueError:
        raise TypeError('The value provided for nb_row must be of type int.')
    try:
        nb_col = int(args.nb_col)
    except ValueError:
        raise TypeError('The value provided for nb_col must be of type int.')
    try:
        obs_prob = float(args.obs_prob)
    except ValueError:
        raise TypeError('The value provided for obs_prob must be of type '
                        'float or int.')
    if obs_prob < 0 or obs_prob > 1:
        raise ValueError('The value provided for obs_prob must be in [0, 1].')

    fs = gen_grid(dir_name, nb_scenarios,
                  dim=(nb_col, nb_row), obs_prop=obs_prob)

    for ind, f in enumerate(fs):
        plot_instance(f, 'data.json')

elif args.graph_type == "star":
    try:
        star_depth = int(args.star_depth)
    except ValueError:
        raise TypeError(
            'The value provided for star_depth must be of type int.')
    try:
        nb_branch = int(args.nb_branch)
    except ValueError:
        raise TypeError(
            'The value provided for nb_branch must be of type int.')
    try:
        obs_prob = float(args.obs_prob)
    except ValueError:
        raise TypeError('The value provided for obs_prob must be of type '
                        'float or int.')
    if obs_prob < 0 or obs_prob > 1:
        raise ValueError('The value provided for obs_prob must be in [0, 1].')

    fs = gen_star(dir_name, nb_scenarios, nb_branch,
                  star_depth, obs_prop=obs_prob)

    for ind, f in enumerate(fs):
        plot_instance(f, 'data.json')
elif args.graph_type == "isthme":
    print('TODO')
elif args.graph_type == "caylus":
    data_path = args.data_path
    if data_path is None:
        data_path = "data/caylus-large.json"        

    f = gen_caylus(dir_name, nb_scenarios, data_path)

    plot_instance(f, 'data.json')
else:
    raise ValueError(f'Unknown graph_type value: "{args.graph_type}".')
