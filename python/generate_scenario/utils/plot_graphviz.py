from networkx.drawing.nx_agraph import to_agraph
import networkx as nx
import json

with open('./data/json_data.json') as outfile:
    data = json.loads(json.load(outfile))

G = nx.readwrite.json_graph.node_link_graph(data['graph'])

for n in G.nodes:
    G.nodes[n]['pos'] = f"{G.nodes[n]['pos'][0]}, {G.nodes[n]['pos'][1]}"

A = to_agraph(G)

A.layout('neato', args='-n2')
A.draw('abcd.png')
