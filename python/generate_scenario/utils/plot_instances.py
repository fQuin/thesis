import networkx as nx
import json
import matplotlib.pyplot as plt
import math
import os
from functools import reduce
import operator

# data_path = ("C:\\Users\\fequi\\Documents\\thesis\\python\\generate_scenario"
#              "\\data\\json_data.json")
#  './data/json_data.json'


def plot_instance(data_path,
                  file_name,
                  plot_nav_graph=True,
                  plot_obs=True,
                  plot_edges_com=True):

    if os.name == 'posix':
        separator = '/'
    else:
        separator = '\\'

    fig, ax = plt.subplots()

    with open(data_path + separator + file_name) as outfile:
        data = json.loads(json.load(outfile))

    G = nx.readwrite.json_graph.node_link_graph(data['graph'])

    top_left_pos = [1e6, -1e6]
    bot_right_pos = [-1e6, 1e6]
    for pos in G.nodes.data('pos'):
        pos = pos[1]
        if pos[0] > bot_right_pos[0]:
            bot_right_pos[0] = pos[0]
        if pos[0] < top_left_pos[0]:
            top_left_pos[0] = pos[0]
        if pos[1] < bot_right_pos[1]:
            bot_right_pos[1] = pos[1]
        if pos[1] > top_left_pos[1]:
            top_left_pos[1] = pos[1]
    height = top_left_pos[1] - max(bot_right_pos[1], 0)
    width = bot_right_pos[0] - max(top_left_pos[0], 0)
    alpha = 0.3
    radius = math.sqrt(alpha*height*width/math.pi/len(G.nodes))

    ax.set_xlim((top_left_pos[0]-radius, bot_right_pos[0]+radius))
    ax.set_ylim((bot_right_pos[1]-radius, top_left_pos[1]+radius))
    plt.gca().set_aspect('equal', adjustable='box')

    for edge in G.edges:
        source_pos = G.nodes[edge[0]]['pos']
        target_pos = G.nodes[edge[1]]['pos']
        ax.plot((source_pos[0], target_pos[0]),
                (source_pos[1], target_pos[1]),
                'k', zorder=-1)

    for pos in G.nodes.data('pos'):
        circle = plt.Circle((pos[1][0], pos[1][1]), radius,
                            fc='w', edgecolor='k')
        ax.add_patch(circle)
        ax.annotate(str(pos[0]), xy=(pos[1][0], pos[1][1]),
                    fontsize=int(radius/(height//100)),
                    ha="center", va="center")

    if plot_obs:
        for points in data['obstacles']:
            node_list = []
            for edge in points:
                if edge[0] not in node_list:
                    node_list.append(edge[0])
                if edge[1] not in node_list:
                    node_list.append(edge[1])
            s1_pos = G.nodes[node_list[0]]["pos"]
            s2_pos = G.nodes[node_list[1]]["pos"]
            s3_pos = G.nodes[node_list[2]]["pos"]
            s4_pos = G.nodes[node_list[3]]["pos"]

            coords = [s1_pos, s2_pos, s3_pos, s4_pos]
            center = tuple(map(operator.truediv, reduce(
                lambda x, y: map(operator.add, x, y), coords),
                [len(coords)] * 2))
            coords = sorted(coords, key=lambda coord: (-135 - math.degrees(
                math.atan2(*tuple(map(
                    operator.sub, coord, center))[::-1]))) % 360)[::-1]
            x = [coords[0][0], coords[1][0], coords[2][0], coords[3][0]]
            y = [coords[0][1], coords[1][1], coords[2][1], coords[3][1]]
            rect = plt.Polygon(xy=list(zip(x, y)),
                               fc='grey', zorder=-2, hatch='\\')
            ax.add_patch(rect)

    fig.savefig(data_path + separator + f'{file_name}_nav_graph.pdf')

    if plot_edges_com:
        for edges, can_com in data['edge_to_edge_com'].items():
            edges = eval(edges)
            if not isinstance(edges[0], tuple):
                continue
            e1_s = G.nodes[edges[0][0]]['pos']
            e1_t = G.nodes[edges[0][1]]['pos']
            e2_s = G.nodes[edges[1][0]]['pos']
            e2_t = G.nodes[edges[1][1]]['pos']
            if can_com == 0:
                e1_middle = ((e1_s[0]+e1_t[0])/2, (e1_s[1]+e1_t[1])/2)
                e2_middle = ((e2_s[0]+e2_t[0])/2, (e2_s[1]+e2_t[1])/2)
                ax.plot((e1_middle[0], e2_middle[0]),
                        (e1_middle[1], e2_middle[1]),
                        'r', alpha=0.5, linewidth=0.25)

        fig.savefig(data_path + separator + f'{file_name}_com_edges.pdf')
    plt.close(fig)


if __name__ == "__main__":
    plot_instance("./data", "caylus_json_data.json")
