"""Work in progress...
"""

import pathlib
import json
import math
import tkinter as tk
import networkx as nx
import tkinter.ttk as ttk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (
        FigureCanvasTkAgg, NavigationToolbar2TkAgg)
from tkinter import filedialog

PROJECT_PATH = pathlib.Path(__file__).parent
PROJECT_UI = PROJECT_PATH / "newproject"


class PlotMrpScenario:
    def __init__(self, master=None):
        # build ui
        self.main = ttk.Frame(master)
        self.image_canvas = tk.Canvas(self.main)
        self.image_canvas.configure(borderwidth='1')
        self.image_canvas.grid(column='0', padx='10', row='0', rowspan='6')
        self.image_on_canvas = self.image_canvas.create_image(0, 0)
        self.import_button = ttk.Button(self.main)
        self.import_button.configure(text='Import scenario')
        self.import_button.grid(column='1', padx='10', row='0')
        self.import_button.configure(command=self.on_click_import_scenario)
        self.plot_navigation_graph = ttk.Checkbutton(self.main)
        self.plot_ng = tk.IntVar(value='')
        self.plot_navigation_graph.configure(offvalue='0', onvalue='1', text='Show Navigation Graph', variable=self.plot_ng)
        self.plot_navigation_graph.grid(column='1', padx='10', row='1')
        self.plot_navigation_graph.configure(command=self.on_check_plot_navigation_graph)
        self.plot_obstacles_button = ttk.Checkbutton(self.main)
        self.plot_obs = tk.IntVar(value='')
        self.plot_obstacles_button.configure(offvalue='0', onvalue='1', text='Show Obstacles', variable=self.plot_obs)
        self.plot_obstacles_button.grid(column='1', padx='10', row='2')
        self.plot_obstacles_button.configure(command=self.on_ckeck_plot_obstacles)
        self.plot_edge_com = ttk.Checkbutton(self.main)
        self.plot_ec = tk.IntVar(value='')
        self.plot_edge_com.configure(offvalue='0', onvalue='1', text='Show Edge-to-Edge Coms', variable=self.plot_ec)
        self.plot_edge_com.grid(column='1', padx='10', row='3')
        self.plot_edge_com.configure(command=self.on_check_plot_edge_com)
        self.save_button = ttk.Button(self.main)
        self.save_button.configure(text='Save Image')
        self.save_button.grid(column='1', row='4')
        self.save_button.configure(command=self.on_click_save)
        self.main.configure(height='800', width='800')
        self.main.grid(column='0', row='0')

        # Main widget
        self.mainwindow = self.main
    
    def run(self):
        self.mainwindow.mainloop()

    def on_click_import_scenario(self):
        self.file_path = filedialog.askopenfilename()
        with open(self.file_path) as outfile:
            data = json.loads(json.load(outfile))
        self.G = nx.readwrite.json_graph.node_link_graph(data['graph'])
        self.obstacles = data['obstacles']
        self.edges_to_edges_com = data['edge_to_edge_com']
        self.fig, self.ax = self.draw_navigation_graph()
        self.image_canvas.draw()
        self.fig.savefig('from_gui.pdf')

    def on_check_plot_navigation_graph(self):
        pass

    def on_ckeck_plot_obstacles(self):
        pass

    def on_check_plot_edge_com(self):
        pass

    def on_click_save(self):
        pass
    
    def draw_navigation_graph(self):
        fig, ax = plt.subplots()
        top_left_pos = [1e6, -1e6]
        bot_right_pos = [-1e6, 1e6]
        for pos in self.G.nodes.data('pos'):
            pos = pos[1]
            if pos[0] > bot_right_pos[0]:
                bot_right_pos[0] = pos[0]
            if pos[0] < top_left_pos[0]:
                top_left_pos[0] = pos[0]
            if pos[1] < bot_right_pos[1]:
                bot_right_pos[1] = pos[1]
            if pos[1] > top_left_pos[1]:
                top_left_pos[1] = pos[1]
        height = top_left_pos[1] - bot_right_pos[1]
        width = bot_right_pos[0] - top_left_pos[0]
        alpha = 0.3     
        radius = math.sqrt(alpha*height*width/math.pi/len(self.G.nodes))
        
        ax.set_xlim((top_left_pos[0]-radius, bot_right_pos[0]+radius))
        ax.set_ylim((bot_right_pos[1]-radius, top_left_pos[1]+radius))
        plt.gca().set_aspect('equal', adjustable='box')
        
        for edge in self.G.edges:
            source_pos = self.G.nodes[edge[0]]['pos']
            target_pos = self.G.nodes[edge[1]]['pos']
            ax.plot((source_pos[0], target_pos[0]),
                    (source_pos[1], target_pos[1]),
                    'k', zorder=-1)
        
            
        for pos in self.G.nodes.data('pos'):
            circle = plt.Circle((pos[1][0], pos[1][1]), radius,
                                fc='w', edgecolor='k')
            ax.add_patch(circle)
            ax.annotate(str(pos[0]), xy=(pos[1][0], pos[1][1]),
                        fontsize=int(radius/1.5), ha="center", va="center")
            
        return (fig, ax)


if __name__ == '__main__':
    root = tk.Tk()
    app = PlotMrpScenario(root)
    app.run()


