# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 21:49:23 2022

@author: fequi
"""

from utils.intersect import Point, lines_intersect
import json
import os
import math
import numpy as np
import networkx as nx


class myStarGraph(nx.Graph):

    def __init__(self, nb_branch, branch_length, obstacles_prob,
                 obstacle_margin=1):
        """ Creates an grid graph with dimensions grid_dimensions.

        The dimension is the length of the list 'grid_dimensions' and the
        size in each dimension is the value of the list element.

        E.g. G=my_grid_graph(grid_dimensions=[2,3]) produces a 2x3 grid graph.

        obstacles_prob indicates the probability of a node to be the upper
        left corner of a square obstacle.

        If periodic=True then join grid edges with periodic boundary
        conditions.

        obstacle_margin is the space between obstacles and graph edges.

        """
        super().__init__(name="grid_graph")

        self.obstacle_margin = obstacle_margin

        self.nbb = nb_branch
        self.blen = branch_length

        self._init_graph()

        self.obstacle_corners = self.select_squares(obstacles_prob)
        self.com_prob_dic = self.make_edge_pair_dic()
        self.node_events = self.create_node_events()

    def _init_graph(self):
        """ Creates a linear graph used to initiliaze the grid graph.
        :param dim: list of integers, the dimensions of the grid graph.
        :param periodic: boolean
        :return G: nx.Graph instance.
        """
        self.graph = nx.generators.classic.star_graph(self.nbb)
        for i in range(self.blen - 1):
            for j in range(self.nbb):
                self.graph.add_edge(j + 1 + i*self.nbb,
                                    j + 1 + (i+1)*self.nbb)

        angles = [i*(math.pi*2/self.nbb) for j in range(self.nbb)
                  for i in range(self.nbb)]
        radii = [100*(i+1) for i in range(self.nbb)
                 for j in range(self.nbb)]

        xs = [r*math.cos(a) for a, r in zip(angles, radii)]
        ys = [r*math.sin(a) for a, r in zip(angles, radii)]

        for ind, n in enumerate(self.graph.nodes):
            if not n == 0:
                self.graph.nodes[n]["pos"] = [xs[ind-1], ys[ind-1]]
            else:
                self.graph.nodes[n]["pos"] = [0, 0]

        for e in self.graph.edges:
            self.graph.edges[e]['weight'] = (
                    (self.graph.nodes[e[0]]['pos'][0]
                     - self.graph.nodes[e[1]]['pos'][0])**2
                    + (self.graph.nodes[e[0]]['pos'][1]
                       - self.graph.nodes[e[1]]['pos'][1])**2)**.5

    def select_squares(self, p, sp=[]):
        """Creates the obstacles
        :param p: float in [0, 1], the probability of a grid cell to be an
          obstacles.
        :param sp: list of 4x2 tuples, a list of obstacles. Stands for square
          points.
        """
        maxnb_squares = self.nbb * (self.blen - 1)
        for x in range(1, len(self.graph.nodes) - self.nbb - 1):
            tmp_prob = np.random.rand()
            if ((x, x + self.nbb), (x + self.nbb, x + self.nbb + 1),
                    (x + self.nbb + 1, x + 1), (x + 1, x)) in sp:
                continue
            if (((x + 1, x + self.nbb + 1),
                    (x + self.nbb + 1, x + self.nbb + 2),
                    (x + self.nbb + 2, x + 2), (x + 2, x + 1)) in sp
                    or ((x-1, x+self.nbb-1), (x+self.nbb-1, x+self.nbb),
                        (x+self.nbb, x), (x, x-1)) in sp
                    or ((x - self.nbb, x), (x, x + 1),
                        (x + 1, x - self.nbb + 1),
                        (x - self.nbb + 1, x - self.nbb)) in sp
                    or ((x + self.nbb, x + 2*self.nbb),
                        (x + 2*self.nbb, x + 2*self.nbb + 1),
                        (x + 2*self.nbb + 1, x + self.nbb + 1),
                        (x + self.nbb + 1, x + self.nbb)) in sp):
                tmp_prob *= 2/3
            else:
                tmp_prob *= 4/3
            if tmp_prob < p:
                sp.append(
                        ((x, x + self.nbb), (x + self.nbb, x + self.nbb + 1),
                         (x + self.nbb + 1, x + 1), (x + 1, x)))
        # Random selection must account for the distribution of the
        # obstacles. If the number of obstacles is an outlier, we
        # must discard them.
        if len(sp) > maxnb_squares*(0.5+1/12**.5):
            return self.select_squares(p, sp=[])
        elif len(sp) < maxnb_squares*(0.5-1/12**.5):
            return self.select_squares(p, sp=sp)
        return sp

    def make_edge_pair_dic(self):
        """Builds a dictionary whose keys are pairs of edges in ExE and values
        are 1 iff there is an obstacle between the edges.
        """
        res = {}
        for e1 in self.graph.edges:
            e1_source_pos = self.graph.nodes[e1[0]]["pos"]
            e1_target_pos = self.graph.nodes[e1[1]]["pos"]
            e1_pos = Point(
                    (e1_source_pos[0]+e1_target_pos[0])/2,
                    (e1_source_pos[1]+e1_target_pos[1])/2)
            for e2 in self.graph.edges:
                if not (e1[0] == e2[0] and e1[1] == e2[1]):
                    e2_source_pos = self.graph.nodes[e2[0]]["pos"]
                    e2_target_pos = self.graph.nodes[e2[1]]["pos"]
                    e2_pos = Point(
                            (e2_source_pos[0]+e2_target_pos[0])/2,
                            (e2_source_pos[1]+e2_target_pos[1])/2)
                    res[str((e1, e2))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        r1 = math.sqrt(s1_pos[0]**2+s1_pos[1]**2)
                        r2 = math.sqrt(s2_pos[0]**2+s2_pos[1]**2)
                        r3 = math.sqrt(s3_pos[0]**2+s3_pos[1]**2)
                        r4 = math.sqrt(s4_pos[0]**2+s4_pos[1]**2)
                        phi1 = math.atan2(s1_pos[1], s1_pos[0])
                        phi2 = math.atan2(s2_pos[1], s2_pos[0])
                        phi3 = math.atan2(s3_pos[1], s3_pos[0])
                        phi4 = math.atan2(s4_pos[1], s4_pos[0])
                        r1 += 5
                        r2 -= 5
                        r3 -= 5
                        r4 += 5
                        phi1 += math.pi/180
                        phi2 += math.pi/180
                        phi3 -= math.pi/180
                        phi4 -= math.pi/180
                        s1_pos = Point(r1*math.cos(phi1), r1*math.sin(phi1))
                        s2_pos = Point(r2*math.cos(phi2), r2*math.sin(phi2))
                        s3_pos = Point(r3*math.cos(phi3), r3*math.sin(phi3))
                        s4_pos = Point(r4*math.cos(phi4), r4*math.sin(phi4))
                        if lines_intersect(
                                (e1_pos, e2_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s3_pos),
                                 (s3_pos, s4_pos),
                                 (s4_pos, s1_pos))):
                            res[str((e1, e2))] = 1
            for n in self.graph.nodes:
                if not (e1[0] == n or e1[1] == n):
                    n_source_pos = self.graph.nodes[n]["pos"]
                    n_pos = Point(n_source_pos[0], n_source_pos[1])
                    res[str((n, e1))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        r1 = math.sqrt(s1_pos[0]**2+s1_pos[1]**2)
                        r2 = math.sqrt(s2_pos[0]**2+s2_pos[1]**2)
                        r3 = math.sqrt(s3_pos[0]**2+s3_pos[1]**2)
                        r4 = math.sqrt(s4_pos[0]**2+s4_pos[1]**2)
                        phi1 = math.atan2(s1_pos[1], s1_pos[0])
                        phi2 = math.atan2(s2_pos[1], s2_pos[0])
                        phi3 = math.atan2(s3_pos[1], s3_pos[0])
                        phi4 = math.atan2(s4_pos[1], s4_pos[0])
                        r1 += 5
                        r2 -= 5
                        r3 -= 5
                        r4 += 5
                        phi1 += math.pi/180
                        phi2 += math.pi/180
                        phi3 -= math.pi/180
                        phi4 -= math.pi/180
                        s1_pos = Point(r1*math.cos(phi1), r1*math.sin(phi1))
                        s2_pos = Point(r2*math.cos(phi2), r2*math.sin(phi2))
                        s3_pos = Point(r3*math.cos(phi3), r3*math.sin(phi3))
                        s4_pos = Point(r4*math.cos(phi4), r4*math.sin(phi4))
                        if lines_intersect(
                                (n_pos, e1_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((n, e1))] = 1
        for n in self.graph.nodes:
            n_source_pos = self.graph.nodes[n]["pos"]
            n_pos = Point(n_source_pos[0], n_source_pos[1])
            for m in self.graph.nodes:
                if not (m == n or m == n):
                    m_source_pos = self.graph.nodes[m]["pos"]
                    m_pos = Point(m_source_pos[0], m_source_pos[1])
                    res[str((n, m))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        r1 = math.sqrt(s1_pos[0]**2+s1_pos[1]**2)
                        r2 = math.sqrt(s2_pos[0]**2+s2_pos[1]**2)
                        r3 = math.sqrt(s3_pos[0]**2+s3_pos[1]**2)
                        r4 = math.sqrt(s4_pos[0]**2+s4_pos[1]**2)
                        phi1 = math.atan2(s1_pos[1], s1_pos[0])
                        phi2 = math.atan2(s2_pos[1], s2_pos[0])
                        phi3 = math.atan2(s3_pos[1], s3_pos[0])
                        phi4 = math.atan2(s4_pos[1], s4_pos[0])
                        r1 += 5
                        r2 -= 5
                        r3 -= 5
                        r4 += 5
                        phi1 += math.pi/180
                        phi2 += math.pi/180
                        phi3 -= math.pi/180
                        phi4 -= math.pi/180
                        s1_pos = Point(r1*math.cos(phi1), r1*math.sin(phi1))
                        s2_pos = Point(r2*math.cos(phi2), r2*math.sin(phi2))
                        s3_pos = Point(r3*math.cos(phi3), r3*math.sin(phi3))
                        s4_pos = Point(r4*math.cos(phi4), r4*math.sin(phi4))
                        if lines_intersect(
                                (n_pos, m_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((n, m))] = 1
        return res

    def create_node_events(self):
        nb_nodes = len(self.graph.nodes)

        night_event_nodes = np.random.choice(
            np.arange(nb_nodes),
            abs(np.random.normal(
                loc=int(nb_nodes/2), scale=2, size=1).astype(int)),
            replace=False)

        event_list = [{
            "event_date": np.random.uniform(50.0, 300.0),
            "event_node": int(event_node),
            "event_type": "edit_sensors",
            "sensor_types": ["lidar", "night_camera"]
            } for event_node in night_event_nodes]

        remaining_nodes = np.delete(np.arange(nb_nodes), night_event_nodes)
        smoke_event_nodes = np.random.choice(
            remaining_nodes,
            abs(np.random.normal(
                loc=int(nb_nodes/4), scale=1, size=1).astype(int)),
            replace=False)

        for event_node in smoke_event_nodes:
            event_list.append({
                "event_date": np.random.uniform(50.0, 900.0),
                "event_node": int(event_node),
                "event_type": "edit_sensors",
                "sensor_types": ["lidar"]
                })
        return event_list

    def save(self, file_path, file_name):
        output_dic = {"graph": nx.readwrite.json_graph.node_link_data(
                self.graph),
                      "obstacles": self.obstacle_corners,
                      "edge_to_edge_com": self.com_prob_dic,
                      "node_events": self.node_events,
                      "blocked_prop": self.blocked_prop()}

        if not os.path.exists(file_path):
            os.makedirs(file_path)
        with open(file_path + file_name, 'w') as outfile:
            json.dump(json.dumps(output_dic), outfile)

    def blocked_prop(self):
        return sum(list(self.com_prob_dic.values())) / len(self.com_prob_dic)


def generate_scenarios(dir_name, nb_scenarios,
                       nb_branch=5, branch_length=5, obs_prop=0.25):
    """Generates nb_scenario scenarios and saves them into dir_name directory.
    """
    if os.name == 'posix':
        separator = '/'
    else:
        separator = '\\'
    dir_addresses = []
    for i in range(nb_scenarios):
        G = myStarGraph(nb_branch, branch_length, obs_prop)
        G.save(dir_name + separator
               + f'star_graphs_nb_branch={nb_branch}_'
               + f'branch_length={branch_length}'
               + f'_obsprop={obs_prop}' + separator + f'scenario_{i}',
               separator + 'data.json')
        dir_addresses.append(
                dir_name + separator
                + f'star_graphs_nb_branch={nb_branch}_'
                + f'branch_length={branch_length}'
                + f'_obsprop={obs_prop}' + separator + f'scenario_{i}')
    return dir_addresses


if __name__ == "__main__":
    if os.name == 'posix':
        sep = '/'
    else:
        sep = '\\'
    G = myStarGraph(8, 4, 0.02)

    G.save(f"..{sep}data{sep}star", f"{sep}json_data.json")
    #  ('./data/json_data.json')
