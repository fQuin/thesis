# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 21:49:23 2022

@author: fequi
"""

from utils.intersect import Point, lines_intersect
from matplotlib import pyplot as plt
import json
import os
import scipy.stats as ss
import numpy as np
import networkx as nx


class myGridGraph(nx.Graph):

    def __init__(self, grid_dimensions, obstacles_prob,
                 periodic=False, obstacle_margin=1):
        """ Creates an grid graph with dimensions grid_dimensions.

        The dimension is the length of the list 'grid_dimensions' and the
        size in each dimension is the value of the list element.

        E.g. G=my_grid_graph(grid_dimensions=[2,3]) produces a 2x3 grid graph.

        obstacles_prob indicates the probability of a node to be the upper
        left corner of a square obstacle.

        If periodic=True then join grid edges with periodic boundary
        conditions.

        obstacle_margin is the space between obstacles and graph edges.

        """
        super().__init__(name="grid_graph")

        self.obstacle_margin = obstacle_margin

        prod = 1
        for d in grid_dimensions:
            prod *= d
        new_labels = [int(i) for i in np.arange(prod)]

        G = self._init_linear_graph(grid_dimensions, periodic)
        self.graph = nx.relabel_nodes(
                G, {n: new_labels[ind] for ind, n in enumerate(G.nodes)})

        self.dim = grid_dimensions

        self.remove_edges((len(self.graph.edges)-len(self.graph.nodes))/2)

        for e in self.graph.edges:
            self.graph.edges[e]['weight'] = (
                    (self.graph.nodes[e[0]]['pos'][0]
                     - self.graph.nodes[e[1]]['pos'][0])**2
                    + (self.graph.nodes[e[0]]['pos'][1]
                       - self.graph.nodes[e[1]]['pos'][1])**2)**.5

        self.obstacle_corners = self.select_squares(obstacles_prob)
        self.com_prob_dic = self.make_edge_pair_dic()
        self.node_events = self.create_node_events()

    def _init_linear_graph(self, dim, periodic):
        """ Creates a linear graph used to initiliaze the grid graph.
        :param dim: list of integers, the dimensions of the grid graph.
        :param periodic: boolean
        :return G: nx.Graph instance.
        """

        if periodic:
            func = nx.cycle_graph
        else:
            func = nx.path_graph

        dim = list(dim)
        current_dim = dim.pop()
        G = func(current_dim)
        while len(dim) > 0:
            current_dim = dim.pop()
            # order matters: copy before it is cleared during the creation of
            Gold = G.copy()
            Gnew = func(current_dim)
            # explicit: create_using=None
            # This is so that we get a new graph of Gnew's class.
            G = nx.cartesian_product(Gnew, Gold)
        # graph G is done but has labels of the form (1,(2,(3,1)))

        for n in G.nodes:
            G.nodes[n]["pos"] = [n[0]*100.0, n[1]*100.0]

        return G

    def remove_edges(self, n, max_it=1e4):
        """Removes random edges from the graph
        :param n: int, the maximum number of edges to remove.
        :param max_it: int, the maximum number of tries.
        """
        nb_removed = 0
        nb_it = 0
        while nb_removed < n and nb_it < max_it:
            H = self.graph.copy()
            h_edge_list = [e for e in self.graph.edges]
            tmp_edge = h_edge_list[
                    int(np.random.choice(np.arange(len(self.graph.edges))))]
            H.remove_edge(*tmp_edge)
            if nx.is_connected(H):
                self.graph = H.copy()
                nb_removed += 1
            nb_it += 1

    def select_squares(self, p, sp=[]):
        """Creates the obstacles
        :param p: float in [0, 1], the probability of a grid cell to be an
          obstacles.
        :param sp: list of 4x2 tuples, a list of obstacles. Stands for square
          points.
        """
        maxnb_squares = (self.dim[0] - 1) * (self.dim[1] - 1)
        for x in self.graph.nodes:
            if (not (x + 1) % self.dim[1] == 0
                    and x + 1 <= len(self.graph.nodes) - self.dim[1]):
                tmp_prob = np.random.rand()
                if ((x, x + self.dim[1]),
                        (x + self.dim[1], x + self.dim[1] + 1),
                        (x + self.dim[1] + 1, x+1), (x+1, x)) in sp:
                    continue
                if (((x + self.dim[1], x + 2*self.dim[1]),
                     (x + 2*self.dim[1], x + 2*self.dim[1] + 1),
                     (x + 2*self.dim[1] + 1, x + self.dim[1] + 1),
                     (x + self.dim[1] + 1, x + self.dim[1])) in sp
                        or ((x + 1, x + self.dim[1] + 1),
                            (x + self.dim[1] + 1, x + self.dim[1] + 2),
                            (x + self.dim[1] + 2, x + 2),
                            (x + 2, x + 1)) in sp
                        or ((x - self.dim[1], x), (x, x + 1),
                            (x + 1, x - self.dim[1] + 1),
                            (x - self.dim[1] + 1, x - self.dim[1])) in sp
                        or ((x - 1, x + self.dim[1] - 1),
                            (x + self.dim[1] - 1, x + self.dim[1]),
                            (x + self.dim[1], x), (x, x - 1)) in sp):
                    tmp_prob *= 2/3
                else:
                    tmp_prob *= 4/3
                if tmp_prob < p:
                    sp.append(
                            ((x, x + self.dim[1]),
                             (x + self.dim[1], x + self.dim[1] + 1),
                             (x + self.dim[1] + 1, x+1), (x+1, x))
                            )
        # Random selection must account for the distribution of the
        # obstacles. If the number of obstacles is an outlier, we
        # must discard them.
        if len(sp) > maxnb_squares*(0.5+1/12**.5):
            return self.select_squares(p, sp=[])
        elif len(sp) < maxnb_squares*(0.5-1/12**.5):
            return self.select_squares(p, sp=sp)
        return sp

    def make_edge_pair_dic(self):
        """Builds a dictionary whose keys are pairs of edges in ExE and values
        are 1 iff there is an obstacle between the edges.
        """
        res = {}
        for e1 in self.graph.edges:
            e1_source_pos = self.graph.nodes[e1[0]]["pos"]
            e1_target_pos = self.graph.nodes[e1[1]]["pos"]
            e1_pos = Point(
                    (e1_source_pos[0]+e1_target_pos[0])/2,
                    (e1_source_pos[1]+e1_target_pos[1])/2)
            for e2 in self.graph.edges:
                if not (e1[0] == e2[0] and e1[1] == e2[1]):
                    e2_source_pos = self.graph.nodes[e2[0]]["pos"]
                    e2_target_pos = self.graph.nodes[e2[1]]["pos"]
                    e2_pos = Point(
                            (e2_source_pos[0]+e2_target_pos[0])/2,
                            (e2_source_pos[1]+e2_target_pos[1])/2)
                    res[str((e1, e2))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        s1_pos = Point(
                                s1_pos[0] + self.obstacle_margin,
                                s1_pos[1] + self.obstacle_margin)
                        s2_pos = Point(
                                s2_pos[0] - self.obstacle_margin,
                                s2_pos[1] + self.obstacle_margin)
                        s3_pos = Point(
                                s3_pos[0] - self.obstacle_margin,
                                s3_pos[1] - self.obstacle_margin)
                        s4_pos = Point(
                                s4_pos[0] + self.obstacle_margin,
                                s4_pos[1] - self.obstacle_margin)
                        if lines_intersect(
                                (e1_pos, e2_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((e1, e2))] = 1
            for n in self.graph.nodes:
                if not (e1[0] == n or e1[1] == n):
                    n_source_pos = self.graph.nodes[n]["pos"]
                    n_pos = Point(n_source_pos[0], n_source_pos[1])
                    res[str((n, e1))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        s1_pos = Point(
                                s1_pos[0] + self.obstacle_margin,
                                s1_pos[1] + self.obstacle_margin)
                        s2_pos = Point(
                                s2_pos[0] - self.obstacle_margin,
                                s2_pos[1] + self.obstacle_margin)
                        s3_pos = Point(
                                s3_pos[0] - self.obstacle_margin,
                                s3_pos[1] - self.obstacle_margin)
                        s4_pos = Point(
                                s4_pos[0] + self.obstacle_margin,
                                s4_pos[1] - self.obstacle_margin)
                        if lines_intersect(
                                (n_pos, e1_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((n, e1))] = 1
        for n in self.graph.nodes:
            n_source_pos = self.graph.nodes[n]["pos"]
            n_pos = Point(n_source_pos[0], n_source_pos[1])
            for m in self.graph.nodes:
                if not (m == n or m == n):
                    m_source_pos = self.graph.nodes[m]["pos"]
                    m_pos = Point(m_source_pos[0], m_source_pos[1])
                    res[str((n, m))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        s1_pos = Point(
                                s1_pos[0] + self.obstacle_margin,
                                s1_pos[1] + self.obstacle_margin)
                        s2_pos = Point(
                                s2_pos[0] - self.obstacle_margin,
                                s2_pos[1] + self.obstacle_margin)
                        s3_pos = Point(
                                s3_pos[0] - self.obstacle_margin,
                                s3_pos[1] - self.obstacle_margin)
                        s4_pos = Point(
                                s4_pos[0] + self.obstacle_margin,
                                s4_pos[1] - self.obstacle_margin)
                        if lines_intersect(
                                (n_pos, m_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((n, m))] = 1
        return res

    def create_node_events(self):
        nb_nodes = len(self.graph.nodes)

        night_event_nodes = np.random.choice(
            np.arange(nb_nodes),
            abs(np.random.normal(
                loc=int(nb_nodes/2), scale=2, size=1).astype(int)),
            replace=False)

        event_list = [{
            "event_date": np.random.uniform(50.0, 900.0),
            "event_node": int(event_node),
            "event_type": "edit_sensors",
            "sensor_types": ["lidar", "night_camera"]
            } for event_node in night_event_nodes]

        remaining_nodes = np.delete(np.arange(nb_nodes), night_event_nodes)
        smoke_event_nodes = np.random.choice(
            remaining_nodes,
            abs(np.random.normal(
                loc=int(nb_nodes/4), scale=1, size=1).astype(int)),
            replace=False)

        for event_node in smoke_event_nodes:
            event_list.append({
                "event_date": np.random.uniform(50.0, 300.0),
                "event_node": int(event_node),
                "event_type": "edit_sensors",
                "sensor_types": ["lidar"]
                })
        return event_list

    def save(self, file_path, file_name):
        output_dic = {
            "graph": nx.readwrite.json_graph.node_link_data(self.graph),
            "obstacles": self.obstacle_corners,
            "edge_to_edge_com": self.com_prob_dic,
            "node_events": self.node_events,
            "blocked_prop": self.blocked_prop()}

        if not os.path.exists(file_path):
            os.makedirs(file_path)
            with open(file_path + file_name, 'w') as outfile:
                json.dump(json.dumps(output_dic), outfile)

    def blocked_prop(self):
        return sum(list(self.com_prob_dic.values())) / len(self.com_prob_dic)


def generate_scenarios(dir_name, nb_scenarios, dim=(4, 5), obs_prop=0.25):
    """Generates nb_scenario scenarios and saves them into dir_name directory.
    """
    if os.name == 'posix':
        separator = '/'
    else:
        separator = '\\'
    dir_addresses = []
    for i in range(nb_scenarios):
        G = myGridGraph(dim, obs_prop)
        G.save(dir_name + separator + f'grid_graphs_dim={dim}'
               + f'_obsprop={obs_prop}' + separator + f'scenario_{i}',
               separator + 'data.json')
        dir_addresses.append(
            dir_name + separator + f'grid_graphs_dim={dim}'
            + f'_obsprop={obs_prop}' + separator + f'scenario_{i}')
    return dir_addresses


if __name__ == "__main__":
    if os.name == 'posix':
        sep = '/'
    else:
        sep = '\\'
    G = myGridGraph((4, 5), 0.25)

    """
    nx.draw(G.graph)
    plt.show()
    """

    G.save(f"C:{sep}Users{sep}fequi{sep}Documents{sep}thesis{sep}python{sep}"
           f"generate_scenario{sep}data{sep}", "json_data.json")
    #  ('./data/json_data.json')
