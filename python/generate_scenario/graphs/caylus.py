# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 21:49:23 2022

@author: fequi
"""

from utils.intersect import Point, lines_intersect
from matplotlib import pyplot as plt
import json
import os
import scipy.stats as ss
import numpy as np
import networkx as nx
import json
import math



class CaylusGraph(nx.Graph):

    def __init__(self, json_path, obstacle_margin=1):
        """

        """
        super().__init__(name="import_graph")

        self.obstacle_margin = obstacle_margin

        with open(json_path) as outfile:
            data = json.load(outfile)
            print(type(data))
        
        G = nx.empty_graph()

        for node in data['nodes']:
            if node["id"] == 0:
                x, y = [200, 200]
                x0, y0 = [200, 200]
                lon1 = node["longitude"]
                lat1 = node["latitude"] 
            else:
                x = x0 - (lon1-node["longitude"])*40000000*math.cos((lat1+node["latitude"])*math.pi/360)/360
                y = y0 - (lat1-node["latitude"])*40000000/360
                
            G.add_node(node["id"], pos=[x, y])
            
        for edge in data["links"]:
            G.add_edge(edge["source"], edge["target"])
        print(G.edges)
        
        self.graph = G
        self.obstacle_corners = [
            [[27, 2], [2, 5], [5, 6], [6, 27]],
            [[4, 5], [5, 2], [2, 3], [3, 4]],
            [[2, 27], [27, 26], [26, 28], [28, 2]],
            [[1, 3], [3, 2], [2, 29], [29, 1]],
            [[36, 28], [28, 42], [42, 40], [40, 36]],
            [[40, 42], [42, 38], [38, 39], [39, 40]],
            [[31, 34], [34, 32], [32, 30], [30, 31]],
            [[30, 0], [0, 37], [37, 41], [41, 30]],
            [[0, 1], [1, 36], [36, 37], [37, 0]],
            [[1, 29], [29, 36], [36, 37], [37, 1]],
            [[27, 2], [2, 5], [5, 6], [6, 27]],
            [[0, 4], [4, 3], [3, 1], [1, 0]],
            [[14, 21], [21, 12], [12, 13], [13, 14]],
            [[15, 14], [14, 13], [13, 16], [16, 15]],
            [[44, 45], [45, 22], [22, 43], [43, 44]],
            [[16, 13], [13, 19], [19, 23], [23, 16]],
            [[23, 19], [19, 18], [18, 35], [35, 23]],
            [[18, 20], [20, 17], [17, 33], [33, 18]],
            [[19, 20], [20, 17], [17, 18], [18, 19]],
            [[19, 13], [13, 12], [12, 20], [20, 19]]
        ]
        self.com_prob_dic = self.make_edge_pair_dic()
        self.node_events = self.create_node_events()

    def make_edge_pair_dic(self):
        """Builds a dictionary whose keys are pairs of edges in ExE and values
        are 1 iff there is an obstacle between the edges.
        """
        res = {}
        for e1 in self.graph.edges:
            e1_source_pos = self.graph.nodes[e1[0]]["pos"]
            e1_target_pos = self.graph.nodes[e1[1]]["pos"]
            e1_pos = Point(
                    (e1_source_pos[0]+e1_target_pos[0])/2,
                    (e1_source_pos[1]+e1_target_pos[1])/2)
            for e2 in self.graph.edges:
                if not (e1[0] == e2[0] and e1[1] == e2[1]):
                    e2_source_pos = self.graph.nodes[e2[0]]["pos"]
                    e2_target_pos = self.graph.nodes[e2[1]]["pos"]
                    e2_pos = Point(
                            (e2_source_pos[0]+e2_target_pos[0])/2,
                            (e2_source_pos[1]+e2_target_pos[1])/2)
                    res[str((e1, e2))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        s1_pos = Point(
                                s1_pos[0] + self.obstacle_margin,
                                s1_pos[1] + self.obstacle_margin)
                        s2_pos = Point(
                                s2_pos[0] - self.obstacle_margin,
                                s2_pos[1] + self.obstacle_margin)
                        s3_pos = Point(
                                s3_pos[0] - self.obstacle_margin,
                                s3_pos[1] - self.obstacle_margin)
                        s4_pos = Point(
                                s4_pos[0] + self.obstacle_margin,
                                s4_pos[1] - self.obstacle_margin)
                        if lines_intersect(
                                (e1_pos, e2_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((e1, e2))] = 1
            for n in self.graph.nodes:
                if not (e1[0] == n or e1[1] == n):
                    n_source_pos = self.graph.nodes[n]["pos"]
                    n_pos = Point(n_source_pos[0], n_source_pos[1])
                    res[str((n, e1))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        s1_pos = Point(
                                s1_pos[0] + self.obstacle_margin,
                                s1_pos[1] + self.obstacle_margin)
                        s2_pos = Point(
                                s2_pos[0] - self.obstacle_margin,
                                s2_pos[1] + self.obstacle_margin)
                        s3_pos = Point(
                                s3_pos[0] - self.obstacle_margin,
                                s3_pos[1] - self.obstacle_margin)
                        s4_pos = Point(
                                s4_pos[0] + self.obstacle_margin,
                                s4_pos[1] - self.obstacle_margin)
                        if lines_intersect(
                                (n_pos, e1_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((n, e1))] = 1
        for n in self.graph.nodes:
            n_source_pos = self.graph.nodes[n]["pos"]
            n_pos = Point(n_source_pos[0], n_source_pos[1])
            for m in self.graph.nodes:
                if not (m == n or m == n):
                    m_source_pos = self.graph.nodes[m]["pos"]
                    m_pos = Point(m_source_pos[0], m_source_pos[1])
                    res[str((n, m))] = 0
                    for obs in self.obstacle_corners:
                        s1_pos = self.graph.nodes[obs[0][0]]["pos"]
                        s2_pos = self.graph.nodes[obs[1][0]]["pos"]
                        s3_pos = self.graph.nodes[obs[2][0]]["pos"]
                        s4_pos = self.graph.nodes[obs[3][0]]["pos"]
                        s1_pos = Point(
                                s1_pos[0] + self.obstacle_margin,
                                s1_pos[1] + self.obstacle_margin)
                        s2_pos = Point(
                                s2_pos[0] - self.obstacle_margin,
                                s2_pos[1] + self.obstacle_margin)
                        s3_pos = Point(
                                s3_pos[0] - self.obstacle_margin,
                                s3_pos[1] - self.obstacle_margin)
                        s4_pos = Point(
                                s4_pos[0] + self.obstacle_margin,
                                s4_pos[1] - self.obstacle_margin)
                        if lines_intersect(
                                (n_pos, m_pos),
                                ((s1_pos, s2_pos),
                                 (s2_pos, s4_pos),
                                 (s1_pos, s3_pos),
                                 (s3_pos, s4_pos))):
                            res[str((n, m))] = 1
        return res

    def create_node_events(self):
        nb_nodes = len(self.graph.nodes)

        night_event_nodes = np.random.choice(
            np.arange(nb_nodes),
            abs(np.random.normal(
                loc=int(nb_nodes/2), scale=2, size=1).astype(int)),
            replace=False)

        event_list = [{
            "event_date": np.random.uniform(50.0, 300.0),
            "event_node": int(event_node),
            "event_type": "edit_sensors",
            "sensor_types": ["lidar", "night_camera"]
            } for event_node in night_event_nodes]

        remaining_nodes = np.delete(np.arange(nb_nodes), night_event_nodes)
        smoke_event_nodes = np.random.choice(
            remaining_nodes,
            abs(np.random.normal(
                loc=int(nb_nodes/4), scale=1, size=1).astype(int)),
            replace=False)

        for event_node in smoke_event_nodes:
            event_list.append({
                "event_date": np.random.uniform(50.0, 300.0),
                "event_node": int(event_node),
                "event_type": "edit_sensors",
                "sensor_types": ["lidar"]
                })
        return event_list

    def save(self, file_path, file_name):
        output_dic = {
            "graph": nx.readwrite.json_graph.node_link_data(self.graph),
            "obstacles": self.obstacle_corners,
            "edge_to_edge_com": self.com_prob_dic,
            "node_events": self.node_events,
            "blocked_prop": self.blocked_prop()}

        if not os.path.exists(file_path):
            print("ici")
            os.makedirs(file_path)
            with open(file_path + file_name, 'w') as outfile:
                json.dump(json.dumps(output_dic), outfile)
        else:
            with open(file_path + file_name, 'w') as outfile:
                json.dump(json.dumps(output_dic), outfile)

    def blocked_prop(self):
        try:
            return sum(list(self.com_prob_dic.values())) / len(self.com_prob_dic)
        except:
            return 0


def generate_scenarios(dir_name, nb_scenarios, data_path):
    """Generates nb_scenario scenarios and saves them into dir_name directory.
    """
    if os.name == 'posix':
        separator = '/'
    else:
        separator = '\\'
    for i in range(nb_scenarios):
        G = CaylusGraph(data_path)
        G.save(dir_name + separator + f'caylus_graphs_large'
               + separator + f'scenario_{i}',
               separator + 'data.json')
    dir_addresses.append(
        dir_name + separator + f'caylus_graphs_large' 
        + separator + f'scenario_0')
    return dir_addresses


if __name__ == "__main__":
    G = CaylusGraph("graphs/caylus-large.json")

    G.save("./data/", "caylus_json_data.json")
