# Generate MRP Scenarios

This package allows the generation and visualization of MRP instances with obstacles and dynamic events.

## Installation

From the installation directory, call
```bash
python setup.py --install
```

## Usage

From the command line, call 
```bash
python make_scenarios.py -h
```
To display the module's options.

Options are as follows:
- --graph_type: The type of navigation graph shape:
                - grid: produces a grid graph, with nb_row and nb_col defined by the corresponding arguments.'
                - star: todo'
                - isthme: todo'
                - caylus: todo')

- --nb_scenarios: Number of scenarios to be generated.

- --dir: Path to the target directory, in which the scenarios will be saved.)

- --nb_row: Number of rows of a grid graph.
- --nb_col: Number of columns of a grid graph.
- --obs_prob: Probability for a node of the graph to be the corner of an obstacle.

## Example

The following creates 50 scenarios on a 5x5 grid graph, with an obstacle density of 0.25. The data scenario i is saved in data/grid_graphs_dim=(5, 5)_obsprop=0.25/scenario_i.
```bash
python make_scenarios.py --nb_scenarios=50 --nb_col=5 --nb_row=5 --obs_prob=0.25 --graph_type=grid --dir=data
```