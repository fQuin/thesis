# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 11:37:40 2022

@author: fequi
"""
import numpy as np
from grid import myGridGraph

def generate_node_events(G, event_proportion=.5,
                         first_event_date=100, last_event_date=600,
                         all_sensors=["day_camera", "night_camera", "lidar"]):
    nodes = [int(n) for n in G.graph.nodes]
    nb_events = int(len(G.graph.nodes)*event_proportion)
    event_locs = np.random.choice(nodes, nb_events, replace=False)
    event_dates = np.random.uniform(
            first_event_date, last_event_date, event_locs.shape)
    event_sensors = [all_sensors]*nb_events
    nb_sensors = len(all_sensors)
    for ind, slist in enumerate(event_sensors):
        ind = np.random.choice(np.arange(1, nb_sensors), p=[0.67, 0.33])
        event_sensors[ind] = slist[ind:]
    event_types = ["edit_sensors"]*nb_events
    return [{"event_date": event_dates[i],
             "event_node": event_locs[i],
             "event_type": event_types[i],
             "sensor_types": event_sensors[i]}
             for i in range(nb_events)]
    
    
if __name__ == "__main__":
    G = myGridGraph((5, 4), 0.33)
    event_dic = generate_node_events(G)
    print(event_dic)