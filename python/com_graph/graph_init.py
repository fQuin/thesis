import networkx as nx
import matplotlib.pyplot as plt

#Create a grid graph
G = nx.grid_graph([3, 3])

#Adding a cost to each edge
for edge in  G.edges:
	G.add_weighted_edges_from([(*edge, 1.0)])

#Drawing the graph
nx.draw(G)
plt.show()

