Titre : Coverage Path Planning for 2D Convex Regions
Auteurs : Juan Irving Vasquez-Gomez1·Magdalena Marciano-Melchor2·Luis Valentin3·Juan Carlos Herrera-Lozada
Mots-clefs : Coverage path planning·Unmanned aerial vehicle·Drone survey·Computational geometry·Optimal path
url : https://link.springer.com/content/pdf/10.1007/s10846-019-01024-y.pdf

Le papier étudie le problème de CPP. La contribution est l'introduction d'un algorithme prenant en compte la distance parcourue depuis le point de départ jusqu'à la zone à couvrire et la distance depuis celle-ci vers le point de récupération du drone.

La méthode est restreinte aux zones de couverture convexe et est conçue pour un unique robot.
Pour couvrir une zone, le drone fait des allers-retours au dessus de celle-ci.

Dans la section suivante, les auteurs démontrent que les points d'entrée et de sortie dans la zone à couvrir, sont antipodaux pour tout chemin de couverture optimal. Sachant que le nombre de points antupodaux est limité à 3/2n, les auteurs proposent une méthode permettant de trouver le BFP pour toute paire de points antipodaux. Cette méthode trouve une paire de lignes de support parrallèles passant par les points considérés, qui ont la distance minimale entre elles, puis calcule le BFP correspondant à ces lignes. La complexité de l'algo est en O(n) !!

Des expériences numériques et des real life expériments permettent de montrer que l'algo est extremement rapide pour trouver un chemin. 
