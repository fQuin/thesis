url : https://www.researchgate.net/profile/Jose_Pinto17/publication/329624239_Multiple_Autonomous_Vehicles_Applied_to_Plume_Detection_and_Tracking/links/5c1b84be458515a4c7eb3fda/Multiple-Autonomous-Vehicles-Applied-to-Plume-Detection-and-Tracking.pdf

mots-clefs : detection, mapping

Le papier se penche en particulier sur une mission consistant à réaliser un mapping du panache d'un fleuve dans l'océan à l'aide de plusieurs UAVs. Les auteurs présentent un environnement de dévellopement pour le planning conçu pour ce cas mais pouvant être étendu à des cas plus généraux. Ce nouvel environnement doit permettre d'accélérer et de faciliter les tâches de programmation, car il offre des outils de hauts niveaux.

Pour mener à bien la mission, les AUVs infèrent le contour du panache en faisant des aller-retours autour de la frontière océan/panache. Plus précisément, les drones avancent tout droit, puis lorsqu'ils détectent qu'ils ont dépassé la frontière en question, tournent selon un angle pré-défini. La détection se fait grâce à un algorithme de reconnaissance d'image.

Les expérimentations en situation réelles ont obtenu de bons résultats, puisque les équipes de robots ont detecté avec succès la frontière océan/panache.


