url : https://journals.sagepub.com/doi/pdf/10.5772/57313

mots-clefs : coordination multi-robot, review, communication, task planning, motion planning, decision making



Cette revue de littérature porte sur le thème de la coordination d'une équipe multi-robot. Les aspects catégorisant les différentes architectures existantes sont énumérés. 

Dans un premier temps, plusieurs types de conflit de ressources sont présentés : les limitations des capacités des robots à communiquer, notamment à cause de limitation dans la vitesse et la quantité de données échangées (toujours d'actualité ??), les conflits spaciaux liés aux passages étroits et aux collisions.

Pour résoudre ces conflits, il est nécéssaires que les robots soient coordonnés. Il existe deux types de coordination. La coordination statique fait référence à un ensemble de règle donné à l'avance aux robots, par exemple, toujours serrer à droite. La coordination dynamique est éxécutée pendant la mission et dépend des évènement rencontrés pendant la mission. Tandis que la rigidité de la coordination statique peu ralentir la completion de la mission, la coordination dynamique peut devenir très exigente en calcul. Une combinaison des deux méthodes est donc nécéssaire pour mener à bien les missions multi-robot.

La communication entre les robots peut se faire de manière explicite, c'est à dire que les robots échangent des informations par le biais de messages qu'ils envoient et reçoivent grâce à un dispositif spécialisé. Les robots peuvent également échangé des informations en modifiant leur environnement. Ces modifications seront alors observées par les autres robots et peuvent constituer des messages que les autres robots pourront interpréter. Les robots peuvent également observer des modifications de l'environnement et en tirer des informations sur l'état des autres robots. On parle alors de communication implicite. En pratique, les deux méthodes sont complémentaires. La communication explicite permet d'échanger des informations complètes mais requiert de plus en plus de communication lorsque le nombre de robots augmente, tandis que la communication implicite peut être incomplète, mais ne requiert pas plus de ressources lorsque le nombre de robots augmente. 

On s'interresse ensuite au planning, qui peut se décomposer en deux étapes : la planification des tâches et la planification des chemins.

La planification de tâches peut elle même être décomposée en deux problèmes : le problème de décomposition des tâches, et le problème d'allocation des tâches. Le problème d'allocation est proche d'un problème de job-shop flexible, mais doit être résolu en ligne pour pouvoir répondre aux évènements imprévus rencontrés pendant la mission. De nombreuses méthodes heuristiques ont été devellopées à cette fin.

Le motion planning pour les missions multi-robots doit tenir compte non seulement des obstacles, mais aussi des autres robots. Il existe plusieurs façon de représenter l'espace dans lequel la mission prend place :

- cell decomposition : nombre finit de cellules.
- potential field : génère un chemin en combinant un terme modélisant l'attraction vers l'objectif et des termes modélisant une répulsion des obstacles.
- roadmap approach : construction d'un graphe dont les noeuds sont les configuration sûres et les arrêtes sont les chemins sûrs.
- rapidly-exploring random tree : construit un arbre en suivant la règle : toute configuration aléatoire sûre est ajoutée en la connectant à la configuration sûre la plus proche dans l'arbre. Cette méthode est utile pour explorer un environnement inconnu.
D'après les auteurs, la dernière méthode, plus récente, présente des avantages sur les trois précédentes, plus datées, en terme d'efficacité et en terme de facilité de mise en place. 

Enfin, la prise de décision peut s'effectuer de manière décentralisée ou de manière centralisée. Les architectures centralisées sont efficace quand le nombre de robots est petit, mais la charge de calcul devient rapidement trop grande lorsque le nombre de robots augmente. De plus, une architecture centralisée est très sensible aux pertes de communication et aux pannes du planner central. en contraste, les architectures décentralisées sont plus robustes mais donnent souvent des solutions sous-optimales. Un middle ground est une architecture hierarchique, dans laquelle l'équipe est scindée en plusieurs sous-équipes qui ont chacune un planner central local. 
