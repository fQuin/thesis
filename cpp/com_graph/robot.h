#include <iostream>
  
// A class that represents a robot
class Robot 
{ 
public:
    int id;
    double x;    // Position in the x-axis
    double y;    // Position in the y-axis
    double c_r;    // communication range
    double s_r;    // scanning range
    Robot(int id, double x, double y, double c_r, double s_r);   // Constructor 
    void move(double a, double b);   // function to move a robot from a point to another
    void printPos();    // prints position 
};
