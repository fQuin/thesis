
// A C++ class for the robots
#include<iostream> 
#include"robot.h"
using namespace std; 


Robot::Robot(int id, double x, double y, double c_r, double s_r) 
{
    this->id = id;
    this->x = x; 
    this->y = y; 
    this->c_r = c_r; 
    this->s_r = s_r; 
}

void Robot::printPos()
{ 
    cout << "Robot position: " << x << " , " << y << "\n";
}

void Robot::move(double a, double b)
{ 
    x = a; 
    y = b;
}

//int main() 
//{
//    Robot r1(0.0, 0.0, 0.5, 2.0);
//	r1.printPos();
//	r1.move(1.0, 1.0);
//	r1.printPos();
//    return 0; 
//} 
