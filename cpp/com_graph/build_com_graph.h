#include <iostream>
#include <list>
#include"robot.h"
using namespace std; 

// A class that represents communication graph
class ComGraph 
{ 
    int V;    // Number of vertices
    list<int> *adj;    // A dynamic array of adjacency lists
    void APUtil(int v, bool visited[], int disc[], int low[],  
                int parent[], bool ap[]); 
    void bridgeUtil(int v, bool visited[], int disc[], int low[], 
                    int parent[]); 
public: 
    ComGraph(int V);   // Constructor 
    void addEdge(int v, int w);   // function to add an edge to graph 
    void printGraph();
    void buildFromRobots(list<Robot> robotList);    // Given a list of Robots, build the graph
    void AP();    // prints articulation points 
    void bridge();    // prints all bridges 
};
