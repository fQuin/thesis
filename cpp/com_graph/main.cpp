#include<iostream>
#include <list>
#include"build_com_graph.h"
using namespace std; 

int main() 
{
    Robot r1(0, 0.0, 0.0, 1.0, 2.0);
    Robot r2(1, 2.0, 0.0, 1.0, 2.0);
    Robot r3(2, 1.0, 1.0, 1.0, 2.0);
    Robot r4(3, 1.0, 0.0, 1.0, 2.0);
	list<Robot> robotList;
	robotList.push_back(r1);
	robotList.push_back(r2);
	robotList.push_back(r3);
	robotList.push_back(r4);
	ComGraph graph(robotList.size());
	graph.buildFromRobots(robotList);
	graph.printGraph();
	graph.AP();
	cout << "\n";
	graph.bridge();
    return 0; 
} 
